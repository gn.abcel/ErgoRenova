#!/bin/bash

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

### Seteo nombre de proceso
PROCESS_NAME="call_center"

### Paths utilizados
CURRENT_DIR=$(dirname ${BASH_SOURCE[0]})
APP_DIR=$CURRENT_DIR/..
PATH_BIN=$APP_DIR/bin
PATH_FUNCTIONS=$PATH_BIN/functions
PATH_CONF=$APP_DIR/conf
PATH_LOG=$APP_DIR/log
PATH_SQL=$APP_DIR/SQL
PATH_TMP=$APP_DIR/TMP
PATH_ARCHIVOS_NO_PROCESADOS=$PATH_TMP/ARCHIVOS_NO_PROCESADOS
PATH_TMP_ARCHIVOS_PROCESADOS=$PATH_TMP/ARCHIVOS_STG_HDFS

### Imports de funciones

. $PATH_FUNCTIONS/send_mail.sh
. $PATH_FUNCTIONS/log.sh


### Leyendo Archivo de Configuracion del proceso
. $PATH_CONF"/"$PROCESS_NAME"_parameter.conf"
MODE=$1

### Obtengo el modo de ejecucuón
AVAILABLE_MODES=$modes
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
	export MODE=$1
else
	echo "Debe ingresar uno de los siguientes modos de ejecucion "$AVAILABLE_MODES
	exit 1
fi

### Leyendo Archivo de Configuracion
. $PATH_CONF"/"$PROCESS_NAME"_"$MODE"_parameter.conf"

hdfs dfs -rm -r -skipTrash $PATH_STG_HDFS'/*'$ext

### Variables internas de fecha
TIMESTAMP_LOG=$(date "+%Y%m%d%H%M%S")
FILE_DATE_N=$2
if [ "$FILE_DATE_N" == "" ]; then
  FILE_DATE_N=$(date --date '-1 day' +%Y%m%d)
fi
CURR_DATE_N=$FILE_DATE_N
CURR_DATE=$(date --date="${FILE_DATE_N}" +%Y-%m-%d)
PROCESS_DATE=$(date "+%Y-%m-%d")

DATE_MAY_01=20180501

### Asuntos de mail
ERROR_MSG="PROD VELEZ - ERROR - $PROCESS_NAME - Modo: $MODE"
FINISH_MSG="PROD VELEZ - OK - $PROCESS_NAME - Modo: $MODE"

### Archivos de log
PROCESS_LOG_FILE="log_"$PROCESS_NAME"_"$TIMESTAMP_LOG".log"
HIVE_LOG_FILE="logHive_"$PROCESS_NAME"_"$TIMESTAMP_LOG".log"
SCREEN_LOG_FILE="logAll_"$PROCESS_NAME"_"$TIMESTAMP_LOG".log"

### Log que captura toda la salida del proceso
exec > $PATH_LOG/$SCREEN_LOG_FILE 2>&1

### Comienzo del proceso
log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Comienzo del proceso $PROCESS_NAME."

## Flujo del proceso
. $PATH_BIN/stg_load_process.sh

## Carga de tablas ORC
. $PATH_BIN/orc_load_process.sh

### Fin del proceso
log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Fin del proceso "$PROCESS_NAME"."

# Se envia el log generado por la corrida al usuario
send_mail "TEXT" "$PATH_LOG/$PROCESS_LOG_FILE" "$FINISH_MSG" "$PATH_LOG/$SCREEN_LOG_FILE"


