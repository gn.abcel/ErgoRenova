#!/bin/bash

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

. $PATH_FUNCTIONS/log.sh
. $PATH_CONF"/"$PROCESS_NAME"_parameter.conf"

log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Se comenzara con la ejecucion de la etapa de STG."

PROCESS_ERROR=0

if [ $MODE == "aoa_callhistory" ]; then 
	
	rm $PATH_TMP'/*'$ext
	
	# Se listan los archivos en $PATH_TMP/files.txt
	smbclient -d 0 $smb_server $smb_pass -U $smb_user -c 'ls AOA*'> $PATH_TMP/files.txt
	
	cat $PATH_TMP/files.txt
	
	# Se calculan la cantidad de lineas para poder eliminar las lineas que no son necesarias. Limpio el archivo
	cantidad_lines_head=$(cat $PATH_TMP/files.txt | wc -l)
	cantidad_lines_head=$((cantidad_lines_head - 2))
	cantidad_lines_tail=$((cantidad_lines_head - 2))
	
	cat $PATH_TMP/files.txt | head -n $cantidad_lines_head | tail -n $cantidad_lines_tail > $PATH_TMP/new_file.txt
	
	cat $PATH_TMP/new_file.txt
	
	# Creo un archivo con la mascara del mes corriente, para validar los archivos procesados
	# command touch no sobreescribe el mismo archivo
	
	# Obtengo la primer columna con el nombre del archivo
	cat $PATH_TMP/new_file.txt | awk '{print $1,$3}' > $PATH_TMP/update_file.txt
	
	cantidad_de_archivos_procesados=0

	# Itero por todos los archivos y los traigo del servidor remoto.
	while read line; do
	
	date_file=$(echo $line | cut -d'_' -f3 | cut -d'.' -f1 | sed -r 's/-/''/g')
	aniomes=${date_file:0:6}
	file_name=$(echo $line | awk '{print $1}')
	file_size=$(echo $line | awk '{print $2}')
	
	# Buscar el archivo en archivo_procesados_$date_file 
	grep $file_name $PATH_TMP_ARCHIVOS_PROCESADOS"/archivos_procesados_"$aniomes".txt"
	
	if [ $? -eq 0 ]; then 
		# Logueo que esta procesado
		log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Archivo ya procesado: $file_name"
	else
		# Logueo que el archivo $file se va procesar.
		log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Se va a procesar el archivo: $file_name"
		smbclient -d 0 $smb_server $smb_pass -U $smb_user -c "get ${file_name} ${PATH_TMP}/${file_name}"
		
			if [ $? -eq 0 ]; then
				log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "El archivo se descargo correctamente: $file_name"
			else
				log 1 "$PATH_LOG/$PROCESS_LOG_FILE" "Error al obtener el archivo: $file_name"
				PROCESS_ERROR=1
				break
			fi
			
		# Validar si el archivo esta corrupto.
		
		# gzip -t file.gz && echo ok || echo bad $PATH_TMP/$line
		peso_del_archivo=$(wc -c $PATH_TMP/$file_name | awk '{print $1}')
		
			if [ -f $file_name -o "$file_size" -eq "$peso_del_archivo" ] ; 
			then 
				# Se carga el archivo en la tabla externa que se encuentra en el path: 
				hdfs dfs -put $PATH_TMP/$file_name $PATH_STG_HDFS
				
				# Se proceso el archivo $file
				# Logueo que esta procesado el archivo $file
				log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Se proceso el archivo: $file_name"
				
				cantidad_de_archivos_procesados=$((cantidad_de_archivos_procesados + 1))
				
				echo $file_name >> $PATH_TMP/$PATH_TMP_ARCHIVOS_PROCESADOS"/archivos_procesados_"$aniomes".txt"
			else 
				log 1 "$PATH_LOG/$PROCESS_LOG_FILE" "Error archivo corrupto : $file_name"
				PROCESS_ERROR=1
			fi
		# Elimino el archivo recien procesado.
		rm $PATH_TMP/$file_name
		
	fi 
	
	done < $PATH_TMP/update_file.txt
	
	log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Cantidad de archivos procesados: $cantidad_de_archivos_procesados "

	
else
	. $PATH_CONF"/"$PROCESS_NAME"_"$MODE"_parameter.conf"
	
	# Descargamos el archivo correspondiente segun el modo y lo subimos al cluster
	
	smbclient -d 0 $smb_server $smb_pass -U $smb_user -c "get ${file_prefix_name}${ext} ${PATH_TMP}/${file_prefix_name}${ext}"
		
		if [ $? -eq 0 ]; then
			log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "El archivo se descargo correctamente: ${file_prefix_name}${ext}"
		else
			log 1 "$PATH_LOG/$PROCESS_LOG_FILE" "Error al obtener el archivo: ${file_prefix_name}${ext}"
			PROCESS_ERROR=1
			exit 1
		fi

		hdfs dfs -put $PATH_TMP/${file_prefix_name}${ext} $PATH_STG_HDFS
		rm $PATH_TMP/${file_prefix_name}${ext}
			
		# Se proceso el archivo $file
		# Logueo que esta procesado el archivo $file
		log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Se proceso el archivo: $file_prefix_name$ext"

		
fi

	if ! [ $PROCESS_ERROR -eq 0 ]; then
		# Se envia el log generado por la corrida al usuario
		FINISH_MSG="PROD VELEZ - WARNING - $PROCESS_NAME"
	fi
	
