set tez.queue.name=${TEZ_QUEUE}; 
truncate table ${DBNAME_WRK_PROD}.f_ps_bonos_sdp_detallada_prev;

-- Creo dos tabla (fija y movil) con los registros cuyos nombre_extractor_tx no aparecen en la lista de los archivos cargados(la tabla anterior) y tambien creo el campo r_number para especificar numerar los registros por ocurrencias.

DROP TABLE IF EXISTS ${DBNAME_WRK_PROD}.altamira_bonos_movil_fp_temp1;
create table ${DBNAME_WRK_PROD}.altamira_bonos_movil_fp_temp1 as select
t0.ani_cd,
t0.tipo_enlace_de,
t0.enlace_de     ,
t0.valor_contador_ca,
t0.fin_promocion_fh,
t0.sesiones_usuario_ca,
t0.identificador_promocion_nr  ,
t0.identificador_tipo_bolton_nr,
t0.costo_renovacion_subperiodo_mo,
t0.ultima_renovacion_fh,
t0.fin_subperiodo_renovado_fh,
t0.unidades_al_renovar_ca,
t0.vencimiento_subperiodo_fh,
t0.nombre_extractor_tx,
 Cast( substr(split(t0.nombre_extractor_tx,'_')[4],1,8) AS INT ) fecha_fh,
ROW_NUMBER() over (PARTITION BY 
t0.ani_cd,
t0.tipo_enlace_de,
t0.enlace_de     ,
t0.valor_contador_ca,
t0.fin_promocion_fh,
t0.sesiones_usuario_ca,
t0.identificador_promocion_nr  ,
t0.identificador_tipo_bolton_nr,
t0.costo_renovacion_subperiodo_mo,
t0.ultima_renovacion_fh,
t0.fin_subperiodo_renovado_fh,
t0.unidades_al_renovar_ca,
t0.vencimiento_subperiodo_fh,
t0.nombre_extractor_tx) as r_number
FROM trafico.altamira_bonos_movil_fp t0 
LEFT JOIN ${DBNAME_EDW_PROD_LOG}.dimensional_list_nombre_extractor t1 on t0.nombre_extractor_tx = t1.nombre_extractor_tx and mode_name ='bonos'
WHERE t1.nombre_extractor_tx IS NULL and fecha = ${CURRENT_DATE_N};


DROP TABLE IF EXISTS ${DBNAME_WRK_PROD}.altamira_bonos_fija_fp_temp1;
create table  ${DBNAME_WRK_PROD}.altamira_bonos_fija_fp_temp1 as select
t0.ani_cd,
t0.tipo_enlace_de,
t0.enlace_de     ,
t0.valor_contador_ca,
t0.fin_promocion_fh,
t0.sesiones_usuario_ca,
t0.identificador_promocion_nr  ,
t0.identificador_tipo_bolton_nr,
t0.costo_renovacion_subperiodo_mo,
t0.ultima_renovacion_fh,
t0.fin_subperiodo_renovado_fh,
t0.unidades_al_renovar_ca,
t0.vencimiento_subperiodo_fh,
t0.nombre_extractor_tx,
 Cast( substr(split(t0.nombre_extractor_tx,'_')[4],1,8) AS INT ) fecha_fh,
ROW_NUMBER() over (PARTITION BY 
t0.ani_cd,
t0.tipo_enlace_de,
t0.enlace_de     ,
t0.valor_contador_ca,
t0.fin_promocion_fh,
t0.sesiones_usuario_ca,
t0.identificador_promocion_nr  ,
t0.identificador_tipo_bolton_nr,
t0.costo_renovacion_subperiodo_mo,
t0.ultima_renovacion_fh,
t0.fin_subperiodo_renovado_fh,
t0.unidades_al_renovar_ca,
t0.vencimiento_subperiodo_fh,
t0.nombre_extractor_tx) as r_number
FROM trafico.altamira_bonos_fija_fp t0
LEFT JOIN ${DBNAME_EDW_PROD_LOG}.dimensional_list_nombre_extractor t1 on t0.nombre_extractor_tx = t1.nombre_extractor_tx and mode_name ='bonos'
WHERE t1.nombre_extractor_tx IS NULL and fecha = ${CURRENT_DATE_N};

---insert  

insert into ${DBNAME_WRK_PROD}.f_ps_bonos_sdp_detallada_prev
select
ani_cd,
tipo_enlace_de,
enlace_de     ,
valor_contador_ca,
fin_promocion_fh,
sesiones_usuario_ca,
identificador_promocion_nr  ,
identificador_tipo_bolton_nr as identificador_tipo_bono_nr,
case when costo_renovacion_subperiodo_mo is not null then costo_renovacion_subperiodo_mo else 0 end costo_renovacion_subperiodo_mo ,     
ultima_renovacion_fh,
fin_subperiodo_renovado_fh,
unidades_al_renovar_ca,
vencimiento_subperiodo_fh,
nombre_extractor_tx,
fecha_fh
from ${DBNAME_WRK_PROD}.altamira_bonos_movil_fp_temp1 
where r_number = 1 
union all
select 
ani_cd,
tipo_enlace_de,
enlace_de,
valor_contador_ca,
fin_promocion_fh,
sesiones_usuario_ca,
identificador_promocion_nr  ,
identificador_tipo_bolton_nr as identificador_tipo_bono_nr,
case when costo_renovacion_subperiodo_mo is not null then costo_renovacion_subperiodo_mo else 0 end costo_renovacion_subperiodo_mo ,     
ultima_renovacion_fh        ,
fin_subperiodo_renovado_fh  ,
unidades_al_renovar_ca      ,
vencimiento_subperiodo_fh   ,
nombre_extractor_tx,
fecha_fh
from ${DBNAME_WRK_PROD}.altamira_bonos_fija_fp_temp1 
where r_number = 1 
union all

select 
ani_cd,
tipo_enlace_de,
enlace_de     ,
valor_contador_ca  ,
fin_promocion_fh   ,
sesiones_usuario_ca,
identificador_promocion_nr  ,
identificador_tipo_bono_nr,
costo_renovacion_subperiodo_mo,
ultima_renovacion_fh,
fin_subperiodo_renovado_fh,
unidades_al_renovar_ca,
vencimiento_subperiodo_fh,
nombre_extractor_tx,
fecha_fh
from ${DBNAME_EDW}.f_ps_bonos_sdp_detallada_reinyect;
