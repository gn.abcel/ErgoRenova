
set tez.queue.name=${TEZ_QUEUE}; 
truncate table ${DBNAME_WRK_PROD}.f_ps_saldos_detallada_prev;

-- Creo dos tabla (fija y movil) con los registros cuyos nombre_extractor_tx no aparecen en la lista de los archivos cargados(la tabla anterior) y tambien creo el campo r_number para especificar numerar los registros por ocurrencias.

DROP TABLE IF EXISTS ${DBNAME_WRK_PROD}.altamira_saldos_movil_fp_temp1;
create table ${DBNAME_WRK_PROD}.altamira_saldos_movil_fp_temp1 as select
t0.ani_cd,
t0.saldo_cd ,
t0.saldo_disponible_mo ,
t0.caducidad_fh ,
t0.saldo_bolsa_2_mo ,
t0.caducidad_2_fh ,
t0.saldo_bolsa_3_mo ,
t0.caducidad_3_fh ,
t0.saldo_bolsa_4_mo ,
t0.caducidad_4_fh ,
t0.saldo_bolsa_5_mo ,
t0.caducidad_5_fh ,
t0.saldo_bolsa_6_mo ,
t0.caducidad_6_fh ,
t0.deuda_pendiente_compensar_mo ,
t0.inicio_deuda_fe,
t0.habilitacion_uso_deuda_fl,
t0.nombre_extractor_tx,
CAST( SUBSTR(split(t0.nombre_extractor_tx,'_')[4],1,8) AS INT )  fecha_id,
ROW_NUMBER() over (PARTITION BY 
t0.ani_cd,
t0.saldo_cd ,
t0.saldo_disponible_mo ,
t0.caducidad_fh ,
t0.saldo_bolsa_2_mo ,
t0.caducidad_2_fh ,
t0.saldo_bolsa_3_mo ,
t0.caducidad_3_fh ,
t0.saldo_bolsa_4_mo ,
t0.caducidad_4_fh ,
t0.saldo_bolsa_5_mo ,
t0.caducidad_5_fh ,
t0.saldo_bolsa_6_mo ,
t0.caducidad_6_fh ,
t0.deuda_pendiente_compensar_mo ,
t0.inicio_deuda_fe,
t0.habilitacion_uso_deuda_fl,
t0.nombre_extractor_tx) as r_number
FROM trafico.altamira_saldos_movil_fp t0 
LEFT JOIN ${DBNAME_EDW_PROD_LOG}.dimensional_list_nombre_extractor t1 on t0.nombre_extractor_tx = t1.nombre_extractor_tx and mode_name ='saldos' 
WHERE t1.nombre_extractor_tx IS NULL and fecha = ${CURRENT_DATE_N};


DROP TABLE IF EXISTS ${DBNAME_WRK_PROD}.altamira_saldos_fija_fp_temp1;
create table  ${DBNAME_WRK_PROD}.altamira_saldos_fija_fp_temp1 as select
t0.ani_cd,
t0.saldo_cd ,
t0.saldo_disponible_mo ,
t0.caducidad_fh ,
t0.saldo_bolsa_2_mo ,
t0.caducidad_2_fh ,
t0.saldo_bolsa_3_mo ,
t0.caducidad_3_fh ,
t0.saldo_bolsa_4_mo ,
t0.caducidad_4_fh ,
t0.saldo_bolsa_5_mo ,
t0.caducidad_5_fh ,
t0.saldo_bolsa_6_mo ,
t0.caducidad_6_fh ,
t0.deuda_pendiente_compensar_mo ,
t0.inicio_deuda_fe,
t0.habilitacion_uso_deuda_fl,
t0.nombre_extractor_tx,
CAST( SUBSTR(split(t0.nombre_extractor_tx,'_')[4],1,8) AS INT )  fecha_id,
ROW_NUMBER() over (PARTITION BY 
t0.ani_cd,
t0.saldo_cd ,
t0.saldo_disponible_mo ,
t0.caducidad_fh ,
t0.saldo_bolsa_2_mo ,
t0.caducidad_2_fh ,
t0.saldo_bolsa_3_mo ,
t0.caducidad_3_fh ,
t0.saldo_bolsa_4_mo ,
t0.caducidad_4_fh ,
t0.saldo_bolsa_5_mo ,
t0.caducidad_5_fh ,
t0.saldo_bolsa_6_mo ,
t0.caducidad_6_fh ,
t0.deuda_pendiente_compensar_mo ,
t0.inicio_deuda_fe,
t0.habilitacion_uso_deuda_fl,
t0.nombre_extractor_tx) as r_number
FROM trafico.altamira_saldos_fija_fp t0
LEFT JOIN ${DBNAME_EDW_PROD_LOG}.dimensional_list_nombre_extractor t1 on t0.nombre_extractor_tx = t1.nombre_extractor_tx and mode_name ='saldos' 
WHERE t1.nombre_extractor_tx IS NULL and fecha = ${CURRENT_DATE_N};

---insert  

insert into ${DBNAME_WRK_PROD}.f_ps_saldos_detallada_prev
select
ani_cd,
saldo_cd ,
saldo_disponible_mo ,
caducidad_fh ,
saldo_bolsa_2_mo ,
caducidad_2_fh ,
saldo_bolsa_3_mo ,
caducidad_3_fh ,
saldo_bolsa_4_mo ,
caducidad_4_fh ,
saldo_bolsa_5_mo ,
caducidad_5_fh ,
saldo_bolsa_6_mo ,
caducidad_6_fh ,
deuda_pendiente_compensar_mo ,
inicio_deuda_fe,
habilitacion_uso_deuda_fl,
nombre_extractor_tx,
fecha_id
from ${DBNAME_WRK_PROD}.altamira_saldos_movil_fp_temp1
where r_number = 1 

union all

select
ani_cd,
saldo_cd ,
saldo_disponible_mo ,
caducidad_fh ,
saldo_bolsa_2_mo ,
caducidad_2_fh ,
saldo_bolsa_3_mo ,
caducidad_3_fh ,
saldo_bolsa_4_mo ,
caducidad_4_fh ,
saldo_bolsa_5_mo ,
caducidad_5_fh ,
saldo_bolsa_6_mo ,
caducidad_6_fh ,
deuda_pendiente_compensar_mo ,
inicio_deuda_fe,
habilitacion_uso_deuda_fl,
nombre_extractor_tx,
fecha_id
from ${DBNAME_WRK_PROD}.altamira_saldos_fija_fp_temp1
where r_number = 1 
union all

select 
ani_cd,
saldo_cd,
saldo_disponible_mo,
caducidad_fh ,
saldo_bolsa_2_mo ,
caducidad_2_fh ,
saldo_bolsa_3_mo ,
caducidad_3_fh ,
saldo_bolsa_4_mo ,
caducidad_4_fh ,
saldo_bolsa_5_mo ,
caducidad_5_fh ,
saldo_bolsa_6_mo ,
caducidad_6_fh ,
deuda_pendiente_compensar_mo ,
inicio_deuda_fe,
habilitacion_uso_deuda_fl,
nombre_extractor_tx,
fecha_id
from ${DBNAME_EDW}.f_ps_saldos_detallada_reinyect;
