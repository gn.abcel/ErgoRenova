set tez.queue.name=batch;

drop table if exists ${DBNAME_EDW}.d_eventprom_contenido_dim;

create table ${DBNAME_EDW}.d_eventprom_contenido_dim stored as orc as 
select
	a.ani_destino_cd,
	a.fecha_mensaje_contenido_fh,
	a.CID_CD,
	case when min(b.id_eventprom_cd) is null  then '-1'
					else
					case when count(*) = 1
								   then min(b.id_eventprom_cd)
								   else 'Multibono' end
					end acumulador_cd					
from
	(select * 
	 from ${DBNAME_WRK_PROD}.f_tr_contenido_detallada_prev) a 
		left join
	(select * 
	 from trafico.altamira_eventprom b
where 
	b.fecha_llamada_fh  > cast(date_sub('${CURRENT_DATE}',270) as timestamp)  -- usar YYYY-MM-DD
	and b.tipo_promocion_cd != 'CR'
	and b.id_eventprom_cd != 16) b
on
	(a.ani_destino_cd = b.ani_cd
	and a.Fecha_Mensaje_Contenido_FH = b.fecha_llamada_fh
	and a.CID_CD = b.cid_cd)
group by
	a.ani_destino_cd,
	a.fecha_mensaje_contenido_fh,
	a.CID_CD
