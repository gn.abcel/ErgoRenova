set tez.queue.name=batch;

drop table if exists ${DBNAME_EDW}.d_eventprom_dim;

create table ${DBNAME_EDW}.d_eventprom_dim stored as orc as 
select
a.ani_cd,
a.inicio_sesion_datos_fh,
a.codigo_identificador_cd,

case when min(b.id_eventprom_cd) is null  then '-1'
                else
                case when count(*) = 1
                               then min(b.id_eventprom_cd)
                               else 'Multibono' end
                end acumulador_cd					
from
(select * from ${DBNAME_WRK_PROD}.f_tr_datos_detallada_prev) a 
	left join
(select * from trafico.altamira_eventprom b 
where 
b.fecha_llamada_fh  > cast(date_sub('${CURRENT_DATE}',270) as timestamp) and 
b.tipo_promocion_cd != 'CR'
and b.id_eventprom_cd != 16
) b

on

(a.ani_cd = b.ani_cd
and
a.inicio_sesion_datos_fh = b.fecha_llamada_fh
and
a.codigo_identificador_cd = b.cid_cd)

group by
a.ani_cd,
a.inicio_sesion_datos_fh,
a.codigo_identificador_cd;
