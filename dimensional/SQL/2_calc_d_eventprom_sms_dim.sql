drop table if exists ${DBNAME_EDW}.d_eventprom_sms_dim;

create table ${DBNAME_EDW}.d_eventprom_sms_dim stored as orc as 
select
	a.Ani_CD,
	a.Fecha_Mensaje_FH,
	a.CID_CD,
	case when min(b.id_eventprom_cd) is null  then '-1'
					else
					case when count(*) = 1
								   then min(b.id_eventprom_cd)
								   else 'Multibono' end
					end acumulador_cd					
from
	(select * 
	 from ${DBNAME_WRK_PROD}.f_tr_sms_detallada_prev) a 
		left join
	(select * 
	 from trafico.altamira_eventprom b
where 
	b.fecha_llamada_fh  > cast(date_sub('${CURRENT_DATE}',400) as timestamp)  -- usar YYYY-MM-DD
	and b.tipo_promocion_cd != 'CR'
	and b.id_eventprom_cd != 16) b
on
	(a.Ani_CD = b.ani_cd
	and a.Fecha_Mensaje_FH = b.fecha_llamada_fh
	and a.CID_CD = b.cid_cd)
group by
	a.Ani_CD,
	a.Fecha_Mensaje_FH,
	a.CID_CD
