drop table wrk_prod.f_tr_contenido_detallada_prev_temp1;
create table wrk_prod.f_tr_contenido_detallada_prev_temp1 as
select
t0.*,
case when t1.acumulador_cd is not null then t1.acumulador_cd else "-3" end acumulador_cd,
case when t2.ambito_registro_id is not null then t2.ambito_registro_id else "-3" end ambito_registro_id,
case when t3.ambito_red_id is not null then t3.ambito_red_id else "-3" end ambito_red_id,
case when t4.sentido_trafico_id is not null then t4.sentido_trafico_id else "-3" end sentido_trafico_id
from
wrk_prod.f_tr_contenido_detallada_prev t0
left join edw_prod.d_eventprom_contenido_dim t1 on t0.ani_destino_cd = t1.ani_destino_cd and t0.Fecha_Mensaje_Contenido_FH = t1.fecha_mensaje_contenido_fh and t0.CID_CD = t1.CID_CD
left join edw_prod.D_Ambito_Registro t2 on t2.Ambito_Registro_CD ='7'
left join edw_prod.D_Ambito_Red t3 on t3.Ambito_Red_CD ='26'
left join edw_prod.D_Sentido_Trafico t4 on t4.Sentido_Trafico_CD ='E';

drop table wrk_prod.f_tr_contenido_detallada_prev_temp2;
create table wrk_prod.f_tr_contenido_detallada_prev_temp2 as
select
t0.*,
case when (moneda_id) is not null then (moneda_id) else "-3" end moneda_tasacion_id,
case when (acumulador_id) is not null then (acumulador_id) else "-3" end Acumulador_Promo_Trafico_ID,
case when t7.sistema_origen_id is not null then t7.sistema_origen_id else "-3" end sistema_origen_id
from
wrk_prod.f_tr_contenido_detallada_prev_temp1 t0
left join edw_prod.D_Moneda t5 on t5.Moneda_CD ='ARS'
left join edw_prod.d_acumulador t6 on t0.acumulador_cd = t6.acumulador_cd
left join edw_prod.d_sistema_origen t7 on t7.sistema_origen_cd ='ALT';

drop table wrk_prod.f_tr_contenido_detallada_prev_temp3;
create table wrk_prod.f_tr_contenido_detallada_prev_temp3 as
select
t0.*,
case when t8.Tramo_Tarifario_ID is not null then t8.Tramo_Tarifario_ID else "-3" end Tramo_Tarifario_ID,
hora_id hora_inicio_mensaje_id
from
wrk_prod.f_tr_contenido_detallada_prev_temp2 t0
left join edw_prod.D_Tramo_Tarifario t8 on case when t0.Costo_Llamada_EURO_MO > 0 then 1 else 0 end = t8.Tramo_Tarifario_CD
left join edw_prod.D_Hora t9 on date_format(t0.Fecha_Mensaje_Contenido_FH,'HH:00') = t9.Hora_24_DE;

drop table wrk_prod.f_tr_contenido_detallada_prev_temp4;
create table wrk_prod.f_tr_contenido_detallada_prev_temp4 as
select
t0.*,
case when (operadora_actual_id) is not null then (operadora_actual_id) else "-3" end operadora_destino_id,
case when t11.Proveedor_Servicio_ID is not null then t11.Proveedor_Servicio_ID else "-3" end Proveedor_Servicio_ID
from
wrk_prod.f_tr_contenido_detallada_prev_temp3 t0
left join edw_prod.ANI_OPERADORAS_DESAGREGADA t10 on t0.Ani_Destino_CD = t10.Ani_CD
left join edw_prod.D_Proveedor_Servicio t11 on t0.Proveedor_Servicio_Original_CD = t11.Proveedor_Servicio_CD;

drop table wrk_prod.f_tr_contenido_detallada_prev_temp6;
create table wrk_prod.f_tr_contenido_detallada_prev_temp6 as
select
t0.*,
md5(concat(t0.Ani_Destino_CD, t0.Fecha_Mensaje_Contenido_FH)) hash_block_6,
case when t12.Contenido_Trafico_ID is not null then t12.Contenido_Trafico_ID else "-3" end Contenido_Trafico_ID,
case when t13.Tipo_Contenido_Trafico_ID is not null then t13.Tipo_Contenido_Trafico_ID else "-3" end Tipo_Contenido_Trafico_ID,
case when t14.tecnologia_acceso_id is not null then t14.tecnologia_acceso_id else "-3" end tecnologia_acceso_id,
case when t15.Estado_Valorizacion_ID is not null then t15.Estado_Valorizacion_ID else "-3" end Estado_Valorizacion_ID
from
wrk_prod.f_tr_contenido_detallada_prev_temp4 t0
left join edw_prod.D_Contenido_Trafico t12 on t0.Contenido_Original_CD = t12.Contenido_Trafico_CD
left join edw_prod.D_Tipo_Contenido_Trafico t13 on t0.Tipo_Contenido_Original_CD = t13.Tipo_Contenido_Trafico_CD
left join edw_prod.D_Tecnologia_Acceso t14 on case when t0.tecnologia_cd in ('0100','0101') then '2' else '-2' end = t14.Tecnologia_Acceso_CD
left  join edw_prod.D_Estado_Valorizacion t15 on case when t0.importe_recarga_mo > 0 then 'CONT_EN' else '-1' end = t15.Estado_Valorizacion_CD;

drop table wrk_prod.f_tr_contenido_detallada_prev_final;
create table wrk_prod.f_tr_contenido_detallada_prev_final as
select
t0.*,
case when t16.suscripcion_id is not null then t16.suscripcion_id else "-3" end suscripcion_id,
case when t16.suscripcion_cd is not null then t16.suscripcion_cd else "-3" end suscripcion_cd,
case when t16.interurbano_cd is not null then t16.interurbano_cd else "-3" end interurbano_cd,
case when t16.urbano_cd is not null then t16.urbano_cd else "-3" end urbano_cd,
case when t16.linea_cd is not null then t16.linea_cd else "-3" end linea_cd,
case when t16.tipo_oferta_id is not null then t16.tipo_oferta_id else "-3" end tipo_oferta_id,
case when t16.tipo_oferta_de is not null then t16.tipo_oferta_de else "Descripcion no disponible" end tipo_oferta_de,
case when t16.catalogo_producto_id is not null then t16.catalogo_producto_id else "-3" end catalogo_producto_id,
case when t16.catalogo_producto_cd is not null then t16.catalogo_producto_cd else "-3" end catalogo_producto_cd,
case when t16.catalogo_producto_de is not null then t16.catalogo_producto_de else "Descripcion no disponible" end catalogo_producto_de,
case when t16.geografia_cnc_id is not null then t16.geografia_cnc_id else "-3" end geografia_cnc_id,
case when t16.geografia_cnc_cd is not null then t16.geografia_cnc_cd else "-3" end geografia_cnc_cd,
case when t16.sub_alm_cd is not null then t16.sub_alm_cd else "-3" end sub_alm_cd,
case when t16.alm_cd is not null then t16.alm_cd else "-3" end alm_cd,
case when t16.sub_region_cnc_id is not null then t16.sub_region_cnc_id else "-3" end sub_region_cnc_id,
case when t16.sub_region_cnc_de is not null then t16.sub_region_cnc_de else "Descripcion no disponible" end sub_region_cnc_de,
case when t16.region_cnc_id is not null then t16.region_cnc_id else "-3" end region_cnc_id,
case when t16.region_cnc_de is not null then t16.region_cnc_de else "Descripcion no disponible" end region_cnc_de,
case when t16.flag_prepago_express_id is not null then t16.flag_prepago_express_id else "-3" end flag_prepago_express_id,
case when t16.operadora_propietaria_id is not null then t16.operadora_propietaria_id else "-3" end operadora_propietaria_id,
case when t16.operadora_propietaria_cd is not null then t16.operadora_propietaria_cd else "-3" end operadora_propietaria_cd,
case when t16.operadora_propietaria_de is not null then t16.operadora_propietaria_de else "Descripcion no disponible" end operadora_propietaria_de,
case when t16.operadora_receptora_id is not null then t16.operadora_receptora_id else "-3" end operadora_receptora_id,
case when t16.operadora_receptora_cd is not null then t16.operadora_receptora_cd else "-3" end operadora_receptora_cd,
case when t16.operadora_receptora_de is not null then t16.operadora_receptora_de else "Descripcion no disponible" end operadora_receptora_de,
case when t16.operadora_donante_id is not null then t16.operadora_donante_id else "-3" end operadora_donante_id,
case when t16.operadora_donante_cd is not null then t16.operadora_donante_cd else "-3" end operadora_donante_cd,
case when t16.operadora_donante_de is not null then t16.operadora_donante_de else "Descripcion no disponible" end operadora_donante_de,
case when t16.flag_volte_id is not null then t16.flag_volte_id else "-3" end flag_volte_id,
case when t16.flag_volte_cd is not null then t16.flag_volte_cd else "-3" end flag_volte_cd,
case when t16.flag_volte_de is not null then t16.flag_volte_de else "Descripcion no disponible" end flag_volte_de,
case when t16.equipo_tecnologia_id is not null then t16.equipo_tecnologia_id else "-3" end equipo_tecnologia_id,
case when t16.equipo_tecnologia_cd is not null then t16.equipo_tecnologia_cd else "-3" end equipo_tecnologia_cd,
case when t16.equipo_tecnologia_de is not null then t16.equipo_tecnologia_de else "Descripcion no disponible" end equipo_tecnologia_de,
case when t16.equipo_utilizado_tecnologia_id is not null then t16.equipo_utilizado_tecnologia_id else "-3" end equipo_utilizado_tecnologia_id,
case when t16.equipo_utilizado_tecnologia_cd is not null then t16.equipo_utilizado_tecnologia_cd else "-3" end equipo_utilizado_tecnologia_cd,
case when t16.equipo_utilizado_tecnologia_de is not null then t16.equipo_utilizado_tecnologia_de else "Descripcion no disponible" end equipo_utilizado_tecnologia_de,
case when t16.catalogo_equipo_id is not null then t16.catalogo_equipo_id else "-3" end catalogo_equipo_id,
case when t16.catalogo_equipo_cd is not null then t16.catalogo_equipo_cd else "-3" end catalogo_equipo_cd,
case when t16.catalogo_equipo_de is not null then t16.catalogo_equipo_de else "Descripcion no disponible" end catalogo_equipo_de,
case when t16.estado_suscripcion_id is not null then t16.estado_suscripcion_id else "-3" end estado_suscripcion_id,
case when t16.estado_suscripcion_cd is not null then t16.estado_suscripcion_cd else "-3" end estado_suscripcion_cd,
case when t16.estado_suscripcion_de is not null then t16.estado_suscripcion_de else "Descripcion no disponible" end estado_suscripcion_de,
case when t16.fecha_estado_suscripcion_id is not null then t16.fecha_estado_suscripcion_id else date("1900-01-01") end fecha_estado_suscripcion_id,
case when t16.cliente_id is not null then t16.cliente_id else "-3" end cliente_id,
case when t16.cliente_cd is not null then t16.cliente_cd else "-3" end cliente_cd,
case when t16.sub_tipo_cliente_id is not null then t16.sub_tipo_cliente_id else "-3" end sub_tipo_cliente_id,
case when t16.sub_tipo_cliente_cd is not null then t16.sub_tipo_cliente_cd else "-3" end sub_tipo_cliente_cd,
case when t16.sub_tipo_cliente_de is not null then t16.sub_tipo_cliente_de else "Descripcion no disponible" end sub_tipo_cliente_de,
case when t16.tipo_cliente_id is not null then t16.tipo_cliente_id else "-3" end tipo_cliente_id,
case when t16.tipo_cliente_cd is not null then t16.tipo_cliente_cd else "-3" end tipo_cliente_cd,
case when t16.tipo_cliente_de is not null then t16.tipo_cliente_de else "Descripcion no disponible" end tipo_cliente_de,
case when t16.ciclo_facturacion_id is not null then t16.ciclo_facturacion_id else "-3" end ciclo_facturacion_id,
case when t16.ciclo_facturacion_cd is not null then t16.ciclo_facturacion_cd else "-3" end ciclo_facturacion_cd,
case when t16.ciclo_facturacion_de is not null then t16.ciclo_facturacion_de else "Descripcion no disponible" end ciclo_facturacion_de,
case when t16.score_economico_id is not null then t16.score_economico_id else "-3" end score_economico_id,
case when t16.score_economico_cd is not null then t16.score_economico_cd else "-3" end score_economico_cd,
case when t16.score_economico_de is not null then t16.score_economico_de else "Descripcion no disponible" end score_economico_de,
case when t16.operadora_larga_distancia_id is not null then t16.operadora_larga_distancia_id else "-3" end operadora_larga_distancia_id,
case when t16.operadora_larga_distancia_cd is not null then t16.operadora_larga_distancia_cd else "-3" end operadora_larga_distancia_cd,
case when t16.operadora_larga_distancia_de is not null then t16.operadora_larga_distancia_de else "Descripcion no disponible" end operadora_larga_distancia_de,
case when t0.ambito_registro_id = "-3" then true else false end or 
case when t0.ambito_red_id = "-3" then true else false end or 
case when t0.sentido_trafico_id = "-3" then true else false end or 
case when t0.moneda_tasacion_id = "-3" then true else false end or 
case when t0.Acumulador_Promo_Trafico_ID = "-3" then true else false end or 
case when t0.sistema_origen_id = "-3" then true else false end or 
case when t0.Tramo_Tarifario_ID = "-3" then true else false end or 
case when t0.operadora_destino_id = "-3" then true else false end or 
case when t0.Proveedor_Servicio_ID = "-3" then true else false end or 
case when t0.Contenido_Trafico_ID = "-3" then true else false end or 
case when t0.Tipo_Contenido_Trafico_ID = "-3" then true else false end or 
case when t0.tecnologia_acceso_id = "-3" then true else false end or 
case when t0.Estado_Valorizacion_ID = "-3" then true else false end or 
case when t16.suscripcion_id is null then true else false end or 
case when t16.tipo_oferta_id is null then true else false end or 
case when t16.catalogo_producto_id is null then true else false end or 
case when t16.geografia_cnc_id is null then true else false end or 
case when t16.sub_region_cnc_id is null then true else false end or 
case when t16.region_cnc_id is null then true else false end or 
case when t16.flag_prepago_express_id is null then true else false end or 
case when t16.estado_suscripcion_id is null then true else false end or 
case when t16.cliente_id is null then true else false end or 
case when t16.sub_tipo_cliente_id is null then true else false end or 
case when t16.tipo_cliente_id is null then true else false end or 
case when t16.ciclo_facturacion_id is null then true else false end or 
case when t16.score_economico_id is null then true else false end reinyectable,
concat(
case when t0.ambito_registro_id = "-3" then "00022," else "" end,
case when t0.ambito_red_id = "-3" then "00027," else "" end,
case when t0.sentido_trafico_id = "-3" then "00029," else "" end,
case when t0.moneda_tasacion_id = "-3" then "00030," else "" end,
case when t0.Acumulador_Promo_Trafico_ID = "-3" then "00032," else "" end,
case when t0.sistema_origen_id = "-3" then "00034," else "" end,
case when t0.Tramo_Tarifario_ID = "-3" then "00036," else "" end,
case when t0.operadora_destino_id = "-3" then "00046," else "" end,
case when t0.Proveedor_Servicio_ID = "-3" then "00043," else "" end,
case when t0.Contenido_Trafico_ID = "-3" then "00044," else "" end,
case when t0.Tipo_Contenido_Trafico_ID = "-3" then "00045," else "" end,
case when t0.tecnologia_acceso_id = "-3" then "00035," else "" end,
case when t0.Estado_Valorizacion_ID = "-3" then "00026," else "" end,
case when t16.suscripcion_id is null then "00001," else "" end,
case when t16.tipo_oferta_id is null then "00002," else "" end,
case when t16.catalogo_producto_id is null then "00003," else "" end,
case when t16.geografia_cnc_id is null then "00004," else "" end,
case when t16.sub_region_cnc_id is null then "00005," else "" end,
case when t16.region_cnc_id is null then "00006," else "" end,
case when t16.flag_prepago_express_id is null then "00007," else "" end,
case when t16.estado_suscripcion_id is null then "00015," else "" end,
case when t16.cliente_id is null then "00016," else "" end,
case when t16.sub_tipo_cliente_id is null then "00017," else "" end,
case when t16.tipo_cliente_id is null then "00018," else "" end,
case when t16.ciclo_facturacion_id is null then "00019," else "" end,
case when t16.score_economico_id is null then "00020" else "" end) error_cd
from
wrk_prod.f_tr_contenido_detallada_prev_temp6 t0
left join edw_prod.suscriptorizacion t16 on t0.Ani_Destino_CD = t16.ani_cd
where
t0.Fecha_Mensaje_Contenido_FH >= t16.inicio_fh and 
t0.Fecha_Mensaje_Contenido_FH <= t16.fin_fh;

insert into wrk_prod.f_tr_contenido_detallada_prev_final 
select
t0.*,
"-3" suscripcion_id,
"-3" suscripcion_cd,
"-3" interurbano_cd,
"-3" urbano_cd,
"-3" linea_cd,
"-3" tipo_oferta_id,
"Descripcion no disponible" tipo_oferta_de,
"-3" catalogo_producto_id,
"-3" catalogo_producto_cd,
"Descripcion no disponible" catalogo_producto_de,
"-3" geografia_cnc_id,
"-3" geografia_cnc_cd,
"-3" sub_alm_cd,
"-3" alm_cd,
"-3" sub_region_cnc_id,
"Descripcion no disponible" sub_region_cnc_de,
"-3" region_cnc_id,
"Descripcion no disponible" region_cnc_de,
"-3" flag_prepago_express_id,
"-3" operadora_propietaria_id,
"-3" operadora_propietaria_cd,
"Descripcion no disponible" operadora_propietaria_de,
"-3" operadora_receptora_id,
"-3" operadora_receptora_cd,
"Descripcion no disponible" operadora_receptora_de,
"-3" operadora_donante_id,
"-3" operadora_donante_cd,
"Descripcion no disponible" operadora_donante_de,
"-3" flag_volte_id,
"-3" flag_volte_cd,
"Descripcion no disponible" flag_volte_de,
"-3" equipo_tecnologia_id,
"-3" equipo_tecnologia_cd,
"Descripcion no disponible" equipo_tecnologia_de,
"-3" equipo_utilizado_tecnologia_id,
"-3" equipo_utilizado_tecnologia_cd,
"Descripcion no disponible" equipo_utilizado_tecnologia_de,
"-3" catalogo_equipo_id,
"-3" catalogo_equipo_cd,
"Descripcion no disponible" catalogo_equipo_de,
"-3" estado_suscripcion_id,
"-3" estado_suscripcion_cd,
"Descripcion no disponible" estado_suscripcion_de,
date("1900-01-01") fecha_estado_suscripcion_id,
"-3" cliente_id,
"-3" cliente_cd,
"-3" sub_tipo_cliente_id,
"-3" sub_tipo_cliente_cd,
"Descripcion no disponible" sub_tipo_cliente_de,
"-3" tipo_cliente_id,
"-3" tipo_cliente_cd,
"Descripcion no disponible" tipo_cliente_de,
"-3" ciclo_facturacion_id,
"-3" ciclo_facturacion_cd,
"Descripcion no disponible" ciclo_facturacion_de,
"-3" score_economico_id,
"-3" score_economico_cd,
"Descripcion no disponible" score_economico_de,
"-3" operadora_larga_distancia_id,
"-3" operadora_larga_distancia_cd,
"Descripcion no disponible" operadora_larga_distancia_de,
true reinyectable,
concat(
case when t0.ambito_registro_id = "-3" then "00022," else "" end,
case when t0.ambito_red_id = "-3" then "00027," else "" end,
case when t0.sentido_trafico_id = "-3" then "00029," else "" end,
case when t0.moneda_tasacion_id = "-3" then "00030," else "" end,
case when t0.Acumulador_Promo_Trafico_ID = "-3" then "00032," else "" end,
case when t0.sistema_origen_id = "-3" then "00034," else "" end,
case when t0.Tramo_Tarifario_ID = "-3" then "00036," else "" end,
case when t0.operadora_destino_id = "-3" then "00046," else "" end,
case when t0.Proveedor_Servicio_ID = "-3" then "00043," else "" end,
case when t0.Contenido_Trafico_ID = "-3" then "00044," else "" end,
case when t0.Tipo_Contenido_Trafico_ID = "-3" then "00045," else "" end,
case when t0.tecnologia_acceso_id = "-3" then "00035," else "" end,
case when t0.Estado_Valorizacion_ID = "-3" then "00026," else "" end,
"00001,",
"00002,",
"00003,",
"00004,",
"00005,",
"00006,",
"00007,",
"00015,",
"00016,",
"00017,",
"00018,",
"00019,",
"00020") error_cd
from wrk_prod.f_tr_contenido_detallada_prev_temp6 t0
where t0.hash_block_6 not in (select distinct hash_block_6 from wrk_prod.f_tr_contenido_detallada_prev_final);

