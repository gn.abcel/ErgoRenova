drop table wrk_prod.f_tr_sms_detallada_prev_temp1;
create table wrk_prod.f_tr_sms_detallada_prev_temp1 as
select
t0.*,
case when t1.Sentido_Trafico_ID is not null then t1.Sentido_Trafico_ID else "-3" end Sentido_Trafico_ID,
case when t1.Sub_Tipo_Mensaje_Trafico_ID is not null then t1.Sub_Tipo_Mensaje_Trafico_ID else "-3" end Sub_Tipo_Mensaje_Trafico_ID,
t1.Tipo_Mensaje_Trafico_CD,
t1.Sentido_Trafico_CD,
case when t1.sentido_trafico_cd = 'S' then t0.Ani_Cd else t0.Ani_Otro_Cd end Ani_Origen_CD,
case when t1.sentido_trafico_cd = 'S' then t0.Ani_Otro_Cd else t0.Ani_Cd end Ani_Destino_CD,
case when (CASE
 WHEN instr(case when t1.sentido_trafico_cd = 'S' then t0.Ani_Cd else t0.Ani_Otro_Cd end, "*") = 1 
  THEN
    1
 ELSE
 2
END) is not null then (CASE
 WHEN instr(case when t1.sentido_trafico_cd = 'S' then t0.Ani_Cd else t0.Ani_Otro_Cd end, "*") = 1 
  THEN
    1
 ELSE
 2
END) else "2" end Numero_Asterisco_Origen_FL,
case when (CASE
 WHEN instr(case when t1.sentido_trafico_cd = 'S' then t0.Ani_Otro_Cd else t0.Ani_Cd end, "*") = 1 
  THEN
   1
  ELSE
 2
END) is not null then (CASE
 WHEN instr(case when t1.sentido_trafico_cd = 'S' then t0.Ani_Otro_Cd else t0.Ani_Cd end, "*") = 1 
  THEN
   1
  ELSE
 2
END) else "2" end Numero_Asterisco_Destino_FL
from
wrk_prod.f_tr_sms_detallada_prev t0
left join edw_prod.D_Sub_Tipo_Mensaje_Trafico_Unif t1 on t0.tipo_trafico_cd = t1.Sub_Tipo_Mensaje_Trafico_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_temp2;
create table wrk_prod.f_tr_sms_detallada_prev_temp2 as
select
t0.*,
case when t2.Sistema_Origen_ID is not null then t2.Sistema_Origen_ID else "-3" end Sistema_Origen_ID,
t3.Numero_Telefonico_Enrutado_cd Ani_Origen_Enrutado_CD,
case when (CASE 
 WHEN t3.Numero_Telefonico_Enrutado_cd IS NULL AND LENGTH(t0.Ani_Origen_CD) < 10
  THEN 1 
 WHEN t3.Numero_Telefonico_Enrutado_cd IS NOT NULL AND LENGTH(t3.Numero_Telefonico_Enrutado_cd) < 10
  THEN 1 
ELSE 2
END) is not null then (CASE 
 WHEN t3.Numero_Telefonico_Enrutado_cd IS NULL AND LENGTH(t0.Ani_Origen_CD) < 10
  THEN 1 
 WHEN t3.Numero_Telefonico_Enrutado_cd IS NOT NULL AND LENGTH(t3.Numero_Telefonico_Enrutado_cd) < 10
  THEN 1 
ELSE 2
END) else "2" end Numero_Corto_Origen_FL
from
wrk_prod.f_tr_sms_detallada_prev_temp1 t0
left join edw_prod.D_Sistema_Origen t2 on t2.Sistema_Origen_CD ='ALT'
left join edw_prod.EXT_ASTERISCO_ENRUTADO t3 on CASE 
   WHEN instr(trim(t0.Ani_Origen_cd), "*") = 1 
   THEN
    trim(t0.Ani_Origen_cd)
 END  = t3.Numero_Telefonico_Asterisco_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_temp3;
create table wrk_prod.f_tr_sms_detallada_prev_temp3 as
select
t0.*,
t4.Numero_Telefonico_Enrutado_cd Ani_Destino_Enrutado_CD,
case when (CASE
 WHEN t4.Numero_Telefonico_Enrutado_cd IS NULL AND LENGTH(t0.Ani_Destino_CD) < 10 
  THEN 1
 WHEN t4.Numero_Telefonico_Enrutado_cd IS NOT NULL AND LENGTH(t4.Numero_Telefonico_Enrutado_cd) < 10 
  THEN 1 
ELSE 2
END) is not null then (CASE
 WHEN t4.Numero_Telefonico_Enrutado_cd IS NULL AND LENGTH(t0.Ani_Destino_CD) < 10 
  THEN 1
 WHEN t4.Numero_Telefonico_Enrutado_cd IS NOT NULL AND LENGTH(t4.Numero_Telefonico_Enrutado_cd) < 10 
  THEN 1 
ELSE 2
END) else "2" end Numero_Corto_Destino_FL
from
wrk_prod.f_tr_sms_detallada_prev_temp2 t0
left join edw_prod.EXT_ASTERISCO_ENRUTADO t4 on CASE 
   WHEN instr(trim(t0.Ani_Destino_cd), "*") = 1 
   THEN
    trim(t0.Ani_Destino_cd)
END = t4.Numero_Telefonico_Asterisco_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_temp4;
create table wrk_prod.f_tr_sms_detallada_prev_temp4 as
select
t0.*,
md5(concat(t0.Ani_Origen_CD, t0.Ani_Destino_CD, cast(substring(t0.Nombre_Archivo_DE,28,8) as int))) hash_block_4,
t5.Homologado_Oper_CD
from
wrk_prod.f_tr_sms_detallada_prev_temp3 t0
left join edw_prod.HOMOLOGA_OPERADORA t5 on t0.Operadora_CD = t5.Codigo_Origen_Oper_CD and t0.Sistema_origen_ID = t5.Sistema_Origen_ID;

drop table wrk_prod.f_tr_sms_detallada_prev_temp5;
create table wrk_prod.f_tr_sms_detallada_prev_temp5 as
select
t0.*,
t6.COD_SERVICIO
from
wrk_prod.f_tr_sms_detallada_prev_temp4 t0
left join TRAFICO.altamira_numeros_free_movil t6 on t0.Ani_Origen_CD = Num_Telefono and t0.Ani_Destino_CD = cad_item and cast(substring(t0.Nombre_Archivo_DE,28,8) as int) = case when 
substring(nombre_extractor_tx,25,8) = fecha 
then fecha
end
where
t0.Fecha_Mensaje_FH >= t6.Fec_Alta;

insert into wrk_prod.f_tr_sms_detallada_prev_temp5 
select
t0.*,
null COD_SERVICIO
from wrk_prod.f_tr_sms_detallada_prev_temp4 t0
where t0.hash_block_4 not in (select distinct hash_block_4 from wrk_prod.f_tr_sms_detallada_prev_temp5);

drop table wrk_prod.f_tr_sms_detallada_prev_temp6;
create table wrk_prod.f_tr_sms_detallada_prev_temp6 as
select
t0.*,
case when (CASE
	  WHEN t0.Ani_Origen_Enrutado_CD IS NULL
	  THEN 
		  CASE
				WHEN ((SUBSTR(t0.Ani_Origen_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Origen_CD, 1, 2) = '08')
				  OR LENGTH(t0.Ani_Origen_CD) < 10
				  OR INSTR(TRIM(t0.Ani_Origen_CD), "*") = 1 )
				THEN 
				 -2
		  ELSE
				Operadora_Actual_ID
		  END 
ELSE
		  CASE
				WHEN ((SUBSTR(t0.Ani_Origen_Enrutado_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Origen_Enrutado_CD, 1, 2) = '08')
				  OR LENGTH(t0.Ani_Origen_Enrutado_CD) < 10
				  OR INSTR(TRIM(t0.Ani_Origen_Enrutado_CD), "*") = 1 )
				THEN 
				 -2
		  ELSE
				Operadora_Actual_ID
		  END 
END) is not null then (CASE
	  WHEN t0.Ani_Origen_Enrutado_CD IS NULL
	  THEN 
		  CASE
				WHEN ((SUBSTR(t0.Ani_Origen_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Origen_CD, 1, 2) = '08')
				  OR LENGTH(t0.Ani_Origen_CD) < 10
				  OR INSTR(TRIM(t0.Ani_Origen_CD), "*") = 1 )
				THEN 
				 -2
		  ELSE
				Operadora_Actual_ID
		  END 
ELSE
		  CASE
				WHEN ((SUBSTR(t0.Ani_Origen_Enrutado_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Origen_Enrutado_CD, 1, 2) = '08')
				  OR LENGTH(t0.Ani_Origen_Enrutado_CD) < 10
				  OR INSTR(TRIM(t0.Ani_Origen_Enrutado_CD), "*") = 1 )
				THEN 
				 -2
		  ELSE
				Operadora_Actual_ID
		  END 
END) else "-3" end Operadora_Origen_ID
from
wrk_prod.f_tr_sms_detallada_prev_temp5 t0
left join edw_prod.ANI_OPERADORAS_DESAGREGADA t7 on CASE  
WHEN t0.Ani_Origen_Enrutado_CD IS NOT NULL
 THEN
   t0.Ani_Origen_Enrutado_CD
 ELSE
   t0.Ani_Origen_CD   
END = t7.Ani_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_temp7;
create table wrk_prod.f_tr_sms_detallada_prev_temp7 as
select
t0.*,
case when (CASE
      WHEN t0.Ani_Destino_Enrutado_CD IS NULL
      THEN 
		  CASE
			WHEN (SUBSTR(t0.Ani_Destino_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Destino_CD, 1, 2) = '08')
				  OR LENGTH(t0.Ani_Destino_CD) < 10
				  OR INSTR(TRIM(t0.Ani_Destino_CD), "*") = 1 
			THEN 
				  -2
			ELSE
				  Operadora_Actual_ID
		  END 
ELSE
      CASE
			WHEN (SUBSTR(t0.Ani_Destino_Enrutado_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Destino_Enrutado_CD, 1, 2) = '08')
			  OR LENGTH(t0.Ani_Destino_Enrutado_CD) < 10
			  OR INSTR(TRIM(t0.Ani_Destino_Enrutado_CD), "*") = 1 
			THEN 
				 -2
				ELSE
				Operadora_Actual_ID
      END 
END) is not null then (CASE
      WHEN t0.Ani_Destino_Enrutado_CD IS NULL
      THEN 
		  CASE
			WHEN (SUBSTR(t0.Ani_Destino_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Destino_CD, 1, 2) = '08')
				  OR LENGTH(t0.Ani_Destino_CD) < 10
				  OR INSTR(TRIM(t0.Ani_Destino_CD), "*") = 1 
			THEN 
				  -2
			ELSE
				  Operadora_Actual_ID
		  END 
ELSE
      CASE
			WHEN (SUBSTR(t0.Ani_Destino_Enrutado_CD, 1, 1) = '8' OR SUBSTR(t0.Ani_Destino_Enrutado_CD, 1, 2) = '08')
			  OR LENGTH(t0.Ani_Destino_Enrutado_CD) < 10
			  OR INSTR(TRIM(t0.Ani_Destino_Enrutado_CD), "*") = 1 
			THEN 
				 -2
				ELSE
				Operadora_Actual_ID
      END 
END) else "-3" end Operadora_Destino_ID,
case when t9.Lista_Destino_ID is not null then t9.Lista_Destino_ID else "-3" end Lista_Destino_ID
from
wrk_prod.f_tr_sms_detallada_prev_temp6 t0
left join edw_prod.ANI_OPERADORAS_DESAGREGADA t8 on CASE 
 WHEN t0.Ani_Destino_Enrutado_CD IS NOT NULL
 THEN
   t0.Ani_Destino_Enrutado_CD
 ELSE
   t0.Ani_Destino_CD   
END = t8.Ani_CD
left join edw_prod.D_Lista_Destino t9 on CASE WHEN t0.COD_SERVICIO IS NOT NULL THEN t0.COD_SERVICIO 
ELSE '-2'
END = trim(t9.Lista_Destino_CD);

drop table wrk_prod.f_tr_sms_detallada_prev_temp8;
create table wrk_prod.f_tr_sms_detallada_prev_temp8 as
select
t0.*,
case when t10.Tribu_Trafico_ID is not null then t10.Tribu_Trafico_ID else "-3" end Tribu_Trafico_ID,
case when t11.Ambito_Registro_ID is not null then t11.Ambito_Registro_ID else "-3" end Ambito_Registro_ID
from
wrk_prod.f_tr_sms_detallada_prev_temp7 t0
left join edw_prod.D_Tribu_Trafico t10 on  CASE
 WHEN Tribu_CD IS NOT NULL AND substring(t0.Nombre_Archivo_DE,24,3) <> 'POS'
  THEN 
		Tribu_CD
 ELSE
		'-2'
 END = t10.Tribu_Trafico_CD
left join edw_prod.D_Ambito_Registro t11 on t11.Ambito_Registro_CD ="9";

drop table wrk_prod.f_tr_sms_detallada_prev_temp9;
create table wrk_prod.f_tr_sms_detallada_prev_temp9 as
select
t0.*,
case when t12.Clase_Descuento_Aplicado_ID is not null then t12.Clase_Descuento_Aplicado_ID else "-3" end Clase_Descuento_Aplicado_ID,
case when t13.Clase_Tarifa_ID is not null then t13.Clase_Tarifa_ID else "-3" end Clase_Tarifa_ID
from
wrk_prod.f_tr_sms_detallada_prev_temp8 t0
left join edw_prod.D_Clase_Descuento_Aplicado t12 on t0.Plan_Tarifario_CD = t12.Clase_Descuento_Aplicado_CD
left join edw_prod.D_Clase_Tarifa t13 on t0.Clase_Tarifa_CD = t13.Clase_Tarifa_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_temp10;
create table wrk_prod.f_tr_sms_detallada_prev_temp10 as
select
t0.*,
case when t14.Detalle_Tipo_Destino_ID is not null then t14.Detalle_Tipo_Destino_ID else "-3" end Detalle_Tipo_Destino_ID,
case when t15.Ambito_Red_ID is not null then t15.Ambito_Red_ID else "-3" end Ambito_Red_ID,
case when (Moneda_Id) is not null then (Moneda_Id) else "-3" end Moneda_Tasacion_ID
from
wrk_prod.f_tr_sms_detallada_prev_temp9 t0
left join edw_prod.D_Detalle_Tipo_Destino t14 on CASE 
 WHEN substring(t0.Nombre_Archivo_DE,24,3) = 'POS'
  THEN
   '-2'
 ELSE
COALESCE(trim(t0.tipo_destino_ext_cd), '-2')
END = t14.Detalle_Tipo_Destino_CD
left join edw_prod.D_Ambito_Red t15 on CASE 
	WHEN (t0.operadora_origen_id = '-2') OR (t0.operadora_destino_id = '-2')
	THEN
		'-2'
	ELSE
		CASE 
			 WHEN t0.operadora_origen_id <> t0.operadora_destino_id
			 THEN
				 '25'
			 ELSE
				 '26'
		 END
END = t15.Ambito_Red_CD
left join edw_prod.D_Moneda t16 on t16.Moneda_CD ='ARS';

drop table wrk_prod.f_tr_sms_detallada_prev_temp11;
create table wrk_prod.f_tr_sms_detallada_prev_temp11 as
select
t0.*,
case when t17.acumulador_cd is not null then t17.acumulador_cd else "-3" end acumulador_cd
from
wrk_prod.f_tr_sms_detallada_prev_temp10 t0
left join edw_prod.d_eventprom_sms_dim t17 on t0.Ani_CD = t17.Ani_CD and t0.Fecha_Mensaje_FH = t17.Fecha_Mensaje_FH and t0.CID_CD = t17.CID_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_temp12;
create table wrk_prod.f_tr_sms_detallada_prev_temp12 as
select
t0.*,
case when (acumulador_id) is not null then (acumulador_id) else "-3" end Acumulador_Promo_Trafico_ID,
case when t19.Tipo_Destino_ID is not null then t19.Tipo_Destino_ID else "-3" end Tipo_Destino_ID
from
wrk_prod.f_tr_sms_detallada_prev_temp11 t0
left join edw_prod.D_Acumulador t18 on t0.acumulador_cd = t18.acumulador_cd
left join edw_prod.D_Tipo_Destino t19 on CASE
  WHEN substring(t0.Nombre_Archivo_DE,24,3) = 'POS'
  THEN
   CASE
    WHEN t0.Tipo_Trafico_CD IN ('MMOVMO', 'MPLAMO', 'MMOVPL', 'MOVMMO', 'PLAMMO', 'MOVMPL')
       THEN
         "9"
    WHEN regexp_replace(t0.Tipo_Trafico_CD," ","") <> ""
       THEN
         "8"
   END
  ELSE
 CASE
   WHEN t0.Tipo_Llamada_CD IN ('MSMS','MSMSM') AND t0.Clase_Tarifa_CD = "60699"
    THEN
      "9"
    ELSE
      "8"
  END
END = t19.Tipo_Destino_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_temp13;
create table wrk_prod.f_tr_sms_detallada_prev_temp13 as
select
t0.*,
md5(concat(T0.Ani_CD, t0.Fecha_Mensaje_FH)) hash_block_13,
case when t20.Tramo_Tarifario_ID is not null then t20.Tramo_Tarifario_ID else "-3" end Tramo_Tarifario_ID,
case when (Hora_ID) is not null then (Hora_ID) else "-3" end Hora_Inicio_Mensaje_ID,
case when t22.Estado_Valorizacion_ID is not null then t22.Estado_Valorizacion_ID else "-3" end Estado_Valorizacion_ID,
case when (CASE
	WHEN indicador_Roaming_cd <> "RMITNCL"
		THEN
			-1
	ELSE
		t23.Operadora_ID
END) is not null then (CASE
	WHEN indicador_Roaming_cd <> "RMITNCL"
		THEN
			-1
	ELSE
		t23.Operadora_ID
END) else "-3" end Operadora_Roaming_ID
from
wrk_prod.f_tr_sms_detallada_prev_temp12 t0
left join edw_prod.D_Tramo_Tarifario t20 on CASE when t0.Costo_Llamada_EURO_MO  > 0 then 1 else 0 end = t20.Tramo_Tarifario_CD
left join edw_prod.D_Hora t21 on date_format(t0.Fecha_Mensaje_FH,'HH:00') = t21.Hora_24_DE
left join edw_prod.D_Estado_Valorizacion t22 on CASE
 WHEN t0.Tipo_Mensaje_Trafico_CD = 'SMS' AND t0.Sentido_Trafico_CD = 'S'
  THEN 
   CASE 
    WHEN COALESCE(Importe_Recarga_MO,0) > 0 
     THEN 'SMS_SA'
    WHEN COALESCE(Importe_Recarga_MO,0) = 0 AND t0.Acumulador_Promo_Trafico_ID NOT IN (-1, -2, -3)
     THEN 'SMSB_SA'
    ELSE
     '-1'
   END
 WHEN t0.Tipo_Mensaje_Trafico_CD = 'SMS' AND t0.Sentido_Trafico_CD = 'E'
  THEN
   CASE
    WHEN COALESCE(Importe_Recarga_MO,0)  > 0
     THEN 'SMS_ON'
    WHEN COALESCE(Importe_Recarga_MO,0) = 0 AND t0.Acumulador_Promo_Trafico_ID not in (-1, -2, -3)
     THEN 'SMSB_ON'
    ELSE
     '-1'   
   END
 WHEN t0.Tipo_Mensaje_Trafico_CD = 'MMS' AND t0.Sentido_Trafico_CD = 'S'
  THEN 
   CASE
    WHEN COALESCE(Importe_Recarga_MO,0)  > 0
     THEN 'MMS_SA'
    WHEN COALESCE(Importe_Recarga_MO,0) = 0 AND t0.Acumulador_Promo_Trafico_ID not in (-1, -2, -3)
     THEN 'MMSB_SA'
    ELSE
     '-1'
   END
 WHEN t0.Tipo_Mensaje_Trafico_CD = 'MMS' AND t0.Sentido_Trafico_CD = 'E'
  THEN 
   CASE
    WHEN COALESCE(Importe_Recarga_MO,0)  > 0
     THEN 'MMS_ON'
    WHEN COALESCE(Importe_Recarga_MO,0) = 0 AND t0.Acumulador_Promo_Trafico_ID not in (-1, -2, -3)
     THEN 'MMSB_ON'
    ELSE
     '-1'
   END
 ELSE
  '-1'
END = Estado_Valorizacion_CD
left join edw_prod.D_Operadora t23 on t0.Homologado_Oper_CD = t23.Operadora_CD;

drop table wrk_prod.f_tr_sms_detallada_prev_final;
create table wrk_prod.f_tr_sms_detallada_prev_final as
select
t0.*,
case when t24.suscripcion_id is not null then t24.suscripcion_id else "-3" end suscripcion_id,
case when t24.suscripcion_cd is not null then t24.suscripcion_cd else "-3" end suscripcion_cd,
case when t24.interurbano_cd is not null then t24.interurbano_cd else "-3" end interurbano_cd,
case when t24.urbano_cd is not null then t24.urbano_cd else "-3" end urbano_cd,
case when t24.linea_cd is not null then t24.linea_cd else "-3" end linea_cd,
case when t24.tipo_oferta_id is not null then t24.tipo_oferta_id else "-3" end tipo_oferta_id,
case when t24.tipo_oferta_de is not null then t24.tipo_oferta_de else "Descripcion no disponible" end tipo_oferta_de,
case when t24.catalogo_producto_id is not null then t24.catalogo_producto_id else "-3" end catalogo_producto_id,
case when t24.catalogo_producto_cd is not null then t24.catalogo_producto_cd else "-3" end catalogo_producto_cd,
case when t24.catalogo_producto_de is not null then t24.catalogo_producto_de else "Descripcion no disponible" end catalogo_producto_de,
case when t24.geografia_cnc_id is not null then t24.geografia_cnc_id else "-3" end geografia_cnc_id,
case when t24.geografia_cnc_cd is not null then t24.geografia_cnc_cd else "-3" end geografia_cnc_cd,
case when t24.sub_alm_cd is not null then t24.sub_alm_cd else "-3" end sub_alm_cd,
case when t24.alm_cd is not null then t24.alm_cd else "-3" end alm_cd,
case when t24.sub_region_cnc_id is not null then t24.sub_region_cnc_id else "-3" end sub_region_cnc_id,
case when t24.sub_region_cnc_de is not null then t24.sub_region_cnc_de else "Descripcion no disponible" end sub_region_cnc_de,
case when t24.region_cnc_id is not null then t24.region_cnc_id else "-3" end region_cnc_id,
case when t24.region_cnc_de is not null then t24.region_cnc_de else "Descripcion no disponible" end region_cnc_de,
case when t24.flag_prepago_express_id is not null then t24.flag_prepago_express_id else "-3" end flag_prepago_express_id,
case when t24.operadora_propietaria_id is not null then t24.operadora_propietaria_id else "-3" end operadora_propietaria_id,
case when t24.operadora_propietaria_cd is not null then t24.operadora_propietaria_cd else "-3" end operadora_propietaria_cd,
case when t24.operadora_propietaria_de is not null then t24.operadora_propietaria_de else "Descripcion no disponible" end operadora_propietaria_de,
case when t24.operadora_receptora_id is not null then t24.operadora_receptora_id else "-3" end operadora_receptora_id,
case when t24.operadora_receptora_cd is not null then t24.operadora_receptora_cd else "-3" end operadora_receptora_cd,
case when t24.operadora_receptora_de is not null then t24.operadora_receptora_de else "Descripcion no disponible" end operadora_receptora_de,
case when t24.operadora_donante_id is not null then t24.operadora_donante_id else "-3" end operadora_donante_id,
case when t24.operadora_donante_cd is not null then t24.operadora_donante_cd else "-3" end operadora_donante_cd,
case when t24.operadora_donante_de is not null then t24.operadora_donante_de else "Descripcion no disponible" end operadora_donante_de,
case when t24.flag_volte_id is not null then t24.flag_volte_id else "-3" end flag_volte_id,
case when t24.flag_volte_cd is not null then t24.flag_volte_cd else "-3" end flag_volte_cd,
case when t24.flag_volte_de is not null then t24.flag_volte_de else "Descripcion no disponible" end flag_volte_de,
case when t24.equipo_tecnologia_id is not null then t24.equipo_tecnologia_id else "-3" end equipo_tecnologia_id,
case when t24.equipo_tecnologia_cd is not null then t24.equipo_tecnologia_cd else "-3" end equipo_tecnologia_cd,
case when t24.equipo_tecnologia_de is not null then t24.equipo_tecnologia_de else "Descripcion no disponible" end equipo_tecnologia_de,
case when t24.equipo_utilizado_tecnologia_id is not null then t24.equipo_utilizado_tecnologia_id else "-3" end equipo_utilizado_tecnologia_id,
case when t24.equipo_utilizado_tecnologia_cd is not null then t24.equipo_utilizado_tecnologia_cd else "-3" end equipo_utilizado_tecnologia_cd,
case when t24.equipo_utilizado_tecnologia_de is not null then t24.equipo_utilizado_tecnologia_de else "Descripcion no disponible" end equipo_utilizado_tecnologia_de,
case when t24.catalogo_equipo_id is not null then t24.catalogo_equipo_id else "-3" end catalogo_equipo_id,
case when t24.catalogo_equipo_cd is not null then t24.catalogo_equipo_cd else "-3" end catalogo_equipo_cd,
case when t24.catalogo_equipo_de is not null then t24.catalogo_equipo_de else "Descripcion no disponible" end catalogo_equipo_de,
case when t24.estado_suscripcion_id is not null then t24.estado_suscripcion_id else "-3" end estado_suscripcion_id,
case when t24.estado_suscripcion_cd is not null then t24.estado_suscripcion_cd else "-3" end estado_suscripcion_cd,
case when t24.estado_suscripcion_de is not null then t24.estado_suscripcion_de else "Descripcion no disponible" end estado_suscripcion_de,
case when t24.fecha_estado_suscripcion_id is not null then t24.fecha_estado_suscripcion_id else date("1900-01-01") end fecha_estado_suscripcion_id,
case when t24.cliente_id is not null then t24.cliente_id else "-3" end cliente_id,
case when t24.cliente_cd is not null then t24.cliente_cd else "-3" end cliente_cd,
case when t24.sub_tipo_cliente_id is not null then t24.sub_tipo_cliente_id else "-3" end sub_tipo_cliente_id,
case when t24.sub_tipo_cliente_cd is not null then t24.sub_tipo_cliente_cd else "-3" end sub_tipo_cliente_cd,
case when t24.sub_tipo_cliente_de is not null then t24.sub_tipo_cliente_de else "Descripcion no disponible" end sub_tipo_cliente_de,
case when t24.tipo_cliente_id is not null then t24.tipo_cliente_id else "-3" end tipo_cliente_id,
case when t24.tipo_cliente_cd is not null then t24.tipo_cliente_cd else "-3" end tipo_cliente_cd,
case when t24.tipo_cliente_de is not null then t24.tipo_cliente_de else "Descripcion no disponible" end tipo_cliente_de,
case when t24.ciclo_facturacion_id is not null then t24.ciclo_facturacion_id else "-3" end ciclo_facturacion_id,
case when t24.ciclo_facturacion_cd is not null then t24.ciclo_facturacion_cd else "-3" end ciclo_facturacion_cd,
case when t24.ciclo_facturacion_de is not null then t24.ciclo_facturacion_de else "Descripcion no disponible" end ciclo_facturacion_de,
case when t24.score_economico_id is not null then t24.score_economico_id else "-3" end score_economico_id,
case when t24.score_economico_cd is not null then t24.score_economico_cd else "-3" end score_economico_cd,
case when t24.score_economico_de is not null then t24.score_economico_de else "Descripcion no disponible" end score_economico_de,
case when t24.operadora_larga_distancia_id is not null then t24.operadora_larga_distancia_id else "-3" end operadora_larga_distancia_id,
case when t24.operadora_larga_distancia_cd is not null then t24.operadora_larga_distancia_cd else "-3" end operadora_larga_distancia_cd,
case when t24.operadora_larga_distancia_de is not null then t24.operadora_larga_distancia_de else "Descripcion no disponible" end operadora_larga_distancia_de,
case when t0.Sentido_Trafico_ID = "-3" then true else false end or 
case when t0.Sub_Tipo_Mensaje_Trafico_ID = "-3" then true else false end or 
case when t0.Sistema_Origen_ID = "-3" then true else false end or 
case when t0.Operadora_Origen_ID = "-3" then true else false end or 
case when t0.Operadora_Destino_ID = "-3" then true else false end or 
case when t0.Lista_Destino_ID = "-3" then true else false end or 
case when t0.Tribu_Trafico_ID = "-3" then true else false end or 
case when t0.Ambito_Registro_ID = "-3" then true else false end or 
case when t0.Clase_Descuento_Aplicado_ID = "-3" then true else false end or 
case when t0.Clase_Tarifa_ID = "-3" then true else false end or 
case when t0.Detalle_Tipo_Destino_ID = "-3" then true else false end or 
case when t0.Ambito_Red_ID = "-3" then true else false end or 
case when t0.Moneda_Tasacion_ID = "-3" then true else false end or 
case when t0.acumulador_cd = "-3" then true else false end or 
case when t0.Acumulador_Promo_Trafico_ID = "-3" then true else false end or 
case when t0.Tipo_Destino_ID = "-3" then true else false end or 
case when t0.Tramo_Tarifario_ID = "-3" then true else false end or 
case when t0.Hora_Inicio_Mensaje_ID = "-3" then true else false end or 
case when t0.Estado_Valorizacion_ID = "-3" then true else false end or 
case when t0.Operadora_Roaming_ID = "-3" then true else false end or 
case when t24.suscripcion_id is null then true else false end or 
case when t24.tipo_oferta_id is null then true else false end or 
case when t24.catalogo_producto_id is null then true else false end or 
case when t24.geografia_cnc_id is null then true else false end or 
case when t24.sub_region_cnc_id is null then true else false end or 
case when t24.region_cnc_id is null then true else false end or 
case when t24.flag_prepago_express_id is null then true else false end or 
case when t24.estado_suscripcion_id is null then true else false end or 
case when t24.cliente_id is null then true else false end or 
case when t24.sub_tipo_cliente_id is null then true else false end or 
case when t24.tipo_cliente_id is null then true else false end or 
case when t24.ciclo_facturacion_id is null then true else false end or 
case when t24.score_economico_id is null then true else false end reinyectable,
concat(
case when t0.Sentido_Trafico_ID = "-3" then "00029," else "" end,
case when t0.Sub_Tipo_Mensaje_Trafico_ID = "-3" then "00050," else "" end,
case when t0.Sistema_Origen_ID = "-3" then "00034," else "" end,
case when t0.Operadora_Origen_ID = "-3" then "00051," else "" end,
case when t0.Operadora_Destino_ID = "-3" then "00046," else "" end,
case when t0.Lista_Destino_ID = "-3" then "00047," else "" end,
case when t0.Tribu_Trafico_ID = "-3" then "00048," else "" end,
case when t0.Ambito_Registro_ID = "-3" then "00022," else "" end,
case when t0.Clase_Descuento_Aplicado_ID = "-3" then "00023," else "" end,
case when t0.Clase_Tarifa_ID = "-3" then "00024," else "" end,
case when t0.Detalle_Tipo_Destino_ID = "-3" then "00052," else "" end,
case when t0.Ambito_Red_ID = "-3" then "00027," else "" end,
case when t0.Moneda_Tasacion_ID = "-3" then "00030," else "" end,
case when t0.acumulador_cd = "-3" then "00032," else "" end,
case when t0.Acumulador_Promo_Trafico_ID = "-3" then "00032," else "" end,
case when t0.Tipo_Destino_ID = "-3" then "00049," else "" end,
case when t0.Tramo_Tarifario_ID = "-3" then "00036," else "" end,
case when t0.Hora_Inicio_Mensaje_ID = "-3" then "00028," else "" end,
case when t0.Estado_Valorizacion_ID = "-3" then "00026," else "" end,
case when t0.Operadora_Roaming_ID = "-3" then "00053," else "" end,
case when t24.suscripcion_id is null then "00001," else "" end,
case when t24.tipo_oferta_id is null then "00002," else "" end,
case when t24.catalogo_producto_id is null then "00003," else "" end,
case when t24.geografia_cnc_id is null then "00004," else "" end,
case when t24.sub_region_cnc_id is null then "00005," else "" end,
case when t24.region_cnc_id is null then "00006," else "" end,
case when t24.flag_prepago_express_id is null then "00007," else "" end,
case when t24.estado_suscripcion_id is null then "00015," else "" end,
case when t24.cliente_id is null then "00016," else "" end,
case when t24.sub_tipo_cliente_id is null then "00017," else "" end,
case when t24.tipo_cliente_id is null then "00018," else "" end,
case when t24.ciclo_facturacion_id is null then "00019," else "" end,
case when t24.score_economico_id is null then "00020" else "" end) error_cd
from
wrk_prod.f_tr_sms_detallada_prev_temp13 t0
left join edw_prod.suscriptorizacion t24 on T0.Ani_CD = t24.ani_cd
where
t0.Fecha_Mensaje_FH >= t24.inicio_fh and 
t0.Fecha_Mensaje_FH <= t24.fin_fh;

insert into wrk_prod.f_tr_sms_detallada_prev_final 
select
t0.*,
"-3" suscripcion_id,
"-3" suscripcion_cd,
"-3" interurbano_cd,
"-3" urbano_cd,
"-3" linea_cd,
"-3" tipo_oferta_id,
"Descripcion no disponible" tipo_oferta_de,
"-3" catalogo_producto_id,
"-3" catalogo_producto_cd,
"Descripcion no disponible" catalogo_producto_de,
"-3" geografia_cnc_id,
"-3" geografia_cnc_cd,
"-3" sub_alm_cd,
"-3" alm_cd,
"-3" sub_region_cnc_id,
"Descripcion no disponible" sub_region_cnc_de,
"-3" region_cnc_id,
"Descripcion no disponible" region_cnc_de,
"-3" flag_prepago_express_id,
"-3" operadora_propietaria_id,
"-3" operadora_propietaria_cd,
"Descripcion no disponible" operadora_propietaria_de,
"-3" operadora_receptora_id,
"-3" operadora_receptora_cd,
"Descripcion no disponible" operadora_receptora_de,
"-3" operadora_donante_id,
"-3" operadora_donante_cd,
"Descripcion no disponible" operadora_donante_de,
"-3" flag_volte_id,
"-3" flag_volte_cd,
"Descripcion no disponible" flag_volte_de,
"-3" equipo_tecnologia_id,
"-3" equipo_tecnologia_cd,
"Descripcion no disponible" equipo_tecnologia_de,
"-3" equipo_utilizado_tecnologia_id,
"-3" equipo_utilizado_tecnologia_cd,
"Descripcion no disponible" equipo_utilizado_tecnologia_de,
"-3" catalogo_equipo_id,
"-3" catalogo_equipo_cd,
"Descripcion no disponible" catalogo_equipo_de,
"-3" estado_suscripcion_id,
"-3" estado_suscripcion_cd,
"Descripcion no disponible" estado_suscripcion_de,
date("1900-01-01") fecha_estado_suscripcion_id,
"-3" cliente_id,
"-3" cliente_cd,
"-3" sub_tipo_cliente_id,
"-3" sub_tipo_cliente_cd,
"Descripcion no disponible" sub_tipo_cliente_de,
"-3" tipo_cliente_id,
"-3" tipo_cliente_cd,
"Descripcion no disponible" tipo_cliente_de,
"-3" ciclo_facturacion_id,
"-3" ciclo_facturacion_cd,
"Descripcion no disponible" ciclo_facturacion_de,
"-3" score_economico_id,
"-3" score_economico_cd,
"Descripcion no disponible" score_economico_de,
"-3" operadora_larga_distancia_id,
"-3" operadora_larga_distancia_cd,
"Descripcion no disponible" operadora_larga_distancia_de,
true reinyectable,
concat(
case when t0.Sentido_Trafico_ID = "-3" then "00029," else "" end,
case when t0.Sub_Tipo_Mensaje_Trafico_ID = "-3" then "00050," else "" end,
case when t0.Sistema_Origen_ID = "-3" then "00034," else "" end,
case when t0.Operadora_Origen_ID = "-3" then "00051," else "" end,
case when t0.Operadora_Destino_ID = "-3" then "00046," else "" end,
case when t0.Lista_Destino_ID = "-3" then "00047," else "" end,
case when t0.Tribu_Trafico_ID = "-3" then "00048," else "" end,
case when t0.Ambito_Registro_ID = "-3" then "00022," else "" end,
case when t0.Clase_Descuento_Aplicado_ID = "-3" then "00023," else "" end,
case when t0.Clase_Tarifa_ID = "-3" then "00024," else "" end,
case when t0.Detalle_Tipo_Destino_ID = "-3" then "00052," else "" end,
case when t0.Ambito_Red_ID = "-3" then "00027," else "" end,
case when t0.Moneda_Tasacion_ID = "-3" then "00030," else "" end,
case when t0.acumulador_cd = "-3" then "00032," else "" end,
case when t0.Acumulador_Promo_Trafico_ID = "-3" then "00032," else "" end,
case when t0.Tipo_Destino_ID = "-3" then "00049," else "" end,
case when t0.Tramo_Tarifario_ID = "-3" then "00036," else "" end,
case when t0.Hora_Inicio_Mensaje_ID = "-3" then "00028," else "" end,
case when t0.Estado_Valorizacion_ID = "-3" then "00026," else "" end,
case when t0.Operadora_Roaming_ID = "-3" then "00053," else "" end,
"00001,",
"00002,",
"00003,",
"00004,",
"00005,",
"00006,",
"00007,",
"00015,",
"00016,",
"00017,",
"00018,",
"00019,",
"00020") error_cd
from wrk_prod.f_tr_sms_detallada_prev_temp13 t0
where t0.hash_block_13 not in (select distinct hash_block_13 from wrk_prod.f_tr_sms_detallada_prev_final);

