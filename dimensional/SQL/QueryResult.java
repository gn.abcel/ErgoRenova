// ORM class for table 'null'
// WARNING: This class is AUTO-GENERATED. Modify at your own risk.
//
// Debug information:
// Generated date: Wed Aug 21 15:46:19 ART 2019
// For connector: org.apache.sqoop.manager.GenericJdbcManager
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;
import com.cloudera.sqoop.lib.JdbcWritableBridge;
import com.cloudera.sqoop.lib.DelimiterSet;
import com.cloudera.sqoop.lib.FieldFormatter;
import com.cloudera.sqoop.lib.RecordParser;
import com.cloudera.sqoop.lib.BooleanParser;
import com.cloudera.sqoop.lib.BlobRef;
import com.cloudera.sqoop.lib.ClobRef;
import com.cloudera.sqoop.lib.LargeObjectLoader;
import com.cloudera.sqoop.lib.SqoopRecord;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class QueryResult extends SqoopRecord  implements DBWritable, Writable {
  private final int PROTOCOL_VERSION = 3;
  public int getClassFormatVersion() { return PROTOCOL_VERSION; }
  public static interface FieldSetterCommand {    void setField(Object value);  }  protected ResultSet __cur_result_set;
  private Map<String, FieldSetterCommand> setters = new HashMap<String, FieldSetterCommand>();
  private void init0() {
    setters.put("start_time", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        start_time = (String)value;
      }
    });
    setters.put("msisdn", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        msisdn = (Long)value;
      }
    });
    setters.put("cell_id", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        cell_id = (String)value;
      }
    });
    setters.put("rat", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        rat = (String)value;
      }
    });
    setters.put("cei_score", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        cei_score = (Integer)value;
      }
    });
    setters.put("old_cei_score", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        old_cei_score = (Integer)value;
      }
    });
    setters.put("imsi", new FieldSetterCommand() {
      @Override
      public void setField(Object value) {
        imsi = (Long)value;
      }
    });
  }
  public QueryResult() {
    init0();
  }
  private String start_time;
  public String get_start_time() {
    return start_time;
  }
  public void set_start_time(String start_time) {
    this.start_time = start_time;
  }
  public QueryResult with_start_time(String start_time) {
    this.start_time = start_time;
    return this;
  }
  private Long msisdn;
  public Long get_msisdn() {
    return msisdn;
  }
  public void set_msisdn(Long msisdn) {
    this.msisdn = msisdn;
  }
  public QueryResult with_msisdn(Long msisdn) {
    this.msisdn = msisdn;
    return this;
  }
  private String cell_id;
  public String get_cell_id() {
    return cell_id;
  }
  public void set_cell_id(String cell_id) {
    this.cell_id = cell_id;
  }
  public QueryResult with_cell_id(String cell_id) {
    this.cell_id = cell_id;
    return this;
  }
  private String rat;
  public String get_rat() {
    return rat;
  }
  public void set_rat(String rat) {
    this.rat = rat;
  }
  public QueryResult with_rat(String rat) {
    this.rat = rat;
    return this;
  }
  private Integer cei_score;
  public Integer get_cei_score() {
    return cei_score;
  }
  public void set_cei_score(Integer cei_score) {
    this.cei_score = cei_score;
  }
  public QueryResult with_cei_score(Integer cei_score) {
    this.cei_score = cei_score;
    return this;
  }
  private Integer old_cei_score;
  public Integer get_old_cei_score() {
    return old_cei_score;
  }
  public void set_old_cei_score(Integer old_cei_score) {
    this.old_cei_score = old_cei_score;
  }
  public QueryResult with_old_cei_score(Integer old_cei_score) {
    this.old_cei_score = old_cei_score;
    return this;
  }
  private Long imsi;
  public Long get_imsi() {
    return imsi;
  }
  public void set_imsi(Long imsi) {
    this.imsi = imsi;
  }
  public QueryResult with_imsi(Long imsi) {
    this.imsi = imsi;
    return this;
  }
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QueryResult)) {
      return false;
    }
    QueryResult that = (QueryResult) o;
    boolean equal = true;
    equal = equal && (this.start_time == null ? that.start_time == null : this.start_time.equals(that.start_time));
    equal = equal && (this.msisdn == null ? that.msisdn == null : this.msisdn.equals(that.msisdn));
    equal = equal && (this.cell_id == null ? that.cell_id == null : this.cell_id.equals(that.cell_id));
    equal = equal && (this.rat == null ? that.rat == null : this.rat.equals(that.rat));
    equal = equal && (this.cei_score == null ? that.cei_score == null : this.cei_score.equals(that.cei_score));
    equal = equal && (this.old_cei_score == null ? that.old_cei_score == null : this.old_cei_score.equals(that.old_cei_score));
    equal = equal && (this.imsi == null ? that.imsi == null : this.imsi.equals(that.imsi));
    return equal;
  }
  public boolean equals0(Object o) {
    if (this == o) {
      return true;
    }
    if (!(o instanceof QueryResult)) {
      return false;
    }
    QueryResult that = (QueryResult) o;
    boolean equal = true;
    equal = equal && (this.start_time == null ? that.start_time == null : this.start_time.equals(that.start_time));
    equal = equal && (this.msisdn == null ? that.msisdn == null : this.msisdn.equals(that.msisdn));
    equal = equal && (this.cell_id == null ? that.cell_id == null : this.cell_id.equals(that.cell_id));
    equal = equal && (this.rat == null ? that.rat == null : this.rat.equals(that.rat));
    equal = equal && (this.cei_score == null ? that.cei_score == null : this.cei_score.equals(that.cei_score));
    equal = equal && (this.old_cei_score == null ? that.old_cei_score == null : this.old_cei_score.equals(that.old_cei_score));
    equal = equal && (this.imsi == null ? that.imsi == null : this.imsi.equals(that.imsi));
    return equal;
  }
  public void readFields(ResultSet __dbResults) throws SQLException {
    this.__cur_result_set = __dbResults;
    this.start_time = JdbcWritableBridge.readString(1, __dbResults);
    this.msisdn = JdbcWritableBridge.readLong(2, __dbResults);
    this.cell_id = JdbcWritableBridge.readString(3, __dbResults);
    this.rat = JdbcWritableBridge.readString(4, __dbResults);
    this.cei_score = JdbcWritableBridge.readInteger(5, __dbResults);
    this.old_cei_score = JdbcWritableBridge.readInteger(6, __dbResults);
    this.imsi = JdbcWritableBridge.readLong(7, __dbResults);
  }
  public void readFields0(ResultSet __dbResults) throws SQLException {
    this.start_time = JdbcWritableBridge.readString(1, __dbResults);
    this.msisdn = JdbcWritableBridge.readLong(2, __dbResults);
    this.cell_id = JdbcWritableBridge.readString(3, __dbResults);
    this.rat = JdbcWritableBridge.readString(4, __dbResults);
    this.cei_score = JdbcWritableBridge.readInteger(5, __dbResults);
    this.old_cei_score = JdbcWritableBridge.readInteger(6, __dbResults);
    this.imsi = JdbcWritableBridge.readLong(7, __dbResults);
  }
  public void loadLargeObjects(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void loadLargeObjects0(LargeObjectLoader __loader)
      throws SQLException, IOException, InterruptedException {
  }
  public void write(PreparedStatement __dbStmt) throws SQLException {
    write(__dbStmt, 0);
  }

  public int write(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(start_time, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeLong(msisdn, 2 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeString(cell_id, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(rat, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(cei_score, 5 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(old_cei_score, 6 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeLong(imsi, 7 + __off, -5, __dbStmt);
    return 7;
  }
  public void write0(PreparedStatement __dbStmt, int __off) throws SQLException {
    JdbcWritableBridge.writeString(start_time, 1 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeLong(msisdn, 2 + __off, -5, __dbStmt);
    JdbcWritableBridge.writeString(cell_id, 3 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeString(rat, 4 + __off, 12, __dbStmt);
    JdbcWritableBridge.writeInteger(cei_score, 5 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeInteger(old_cei_score, 6 + __off, 4, __dbStmt);
    JdbcWritableBridge.writeLong(imsi, 7 + __off, -5, __dbStmt);
  }
  public void readFields(DataInput __dataIn) throws IOException {
this.readFields0(__dataIn);  }
  public void readFields0(DataInput __dataIn) throws IOException {
    if (__dataIn.readBoolean()) { 
        this.start_time = null;
    } else {
    this.start_time = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.msisdn = null;
    } else {
    this.msisdn = Long.valueOf(__dataIn.readLong());
    }
    if (__dataIn.readBoolean()) { 
        this.cell_id = null;
    } else {
    this.cell_id = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.rat = null;
    } else {
    this.rat = Text.readString(__dataIn);
    }
    if (__dataIn.readBoolean()) { 
        this.cei_score = null;
    } else {
    this.cei_score = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.old_cei_score = null;
    } else {
    this.old_cei_score = Integer.valueOf(__dataIn.readInt());
    }
    if (__dataIn.readBoolean()) { 
        this.imsi = null;
    } else {
    this.imsi = Long.valueOf(__dataIn.readLong());
    }
  }
  public void write(DataOutput __dataOut) throws IOException {
    if (null == this.start_time) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, start_time);
    }
    if (null == this.msisdn) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.msisdn);
    }
    if (null == this.cell_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cell_id);
    }
    if (null == this.rat) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, rat);
    }
    if (null == this.cei_score) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.cei_score);
    }
    if (null == this.old_cei_score) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.old_cei_score);
    }
    if (null == this.imsi) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.imsi);
    }
  }
  public void write0(DataOutput __dataOut) throws IOException {
    if (null == this.start_time) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, start_time);
    }
    if (null == this.msisdn) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.msisdn);
    }
    if (null == this.cell_id) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, cell_id);
    }
    if (null == this.rat) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    Text.writeString(__dataOut, rat);
    }
    if (null == this.cei_score) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.cei_score);
    }
    if (null == this.old_cei_score) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeInt(this.old_cei_score);
    }
    if (null == this.imsi) { 
        __dataOut.writeBoolean(true);
    } else {
        __dataOut.writeBoolean(false);
    __dataOut.writeLong(this.imsi);
    }
  }
  private static final DelimiterSet __outputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 0, false);
  public String toString() {
    return toString(__outputDelimiters, true);
  }
  public String toString(DelimiterSet delimiters) {
    return toString(delimiters, true);
  }
  public String toString(boolean useRecordDelim) {
    return toString(__outputDelimiters, useRecordDelim);
  }
  public String toString(DelimiterSet delimiters, boolean useRecordDelim) {
    StringBuilder __sb = new StringBuilder();
    char fieldDelim = delimiters.getFieldsTerminatedBy();
    __sb.append(FieldFormatter.escapeAndEnclose(start_time==null?"null":start_time, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(msisdn==null?"null":"" + msisdn, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cell_id==null?"null":cell_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rat==null?"null":rat, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cei_score==null?"null":"" + cei_score, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(old_cei_score==null?"null":"" + old_cei_score, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(imsi==null?"null":"" + imsi, delimiters));
    if (useRecordDelim) {
      __sb.append(delimiters.getLinesTerminatedBy());
    }
    return __sb.toString();
  }
  public void toString0(DelimiterSet delimiters, StringBuilder __sb, char fieldDelim) {
    __sb.append(FieldFormatter.escapeAndEnclose(start_time==null?"null":start_time, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(msisdn==null?"null":"" + msisdn, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cell_id==null?"null":cell_id, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(rat==null?"null":rat, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(cei_score==null?"null":"" + cei_score, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(old_cei_score==null?"null":"" + old_cei_score, delimiters));
    __sb.append(fieldDelim);
    __sb.append(FieldFormatter.escapeAndEnclose(imsi==null?"null":"" + imsi, delimiters));
  }
  private static final DelimiterSet __inputDelimiters = new DelimiterSet((char) 44, (char) 10, (char) 0, (char) 0, false);
  private RecordParser __parser;
  public void parse(Text __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharSequence __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(byte [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(char [] __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(ByteBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  public void parse(CharBuffer __record) throws RecordParser.ParseError {
    if (null == this.__parser) {
      this.__parser = new RecordParser(__inputDelimiters);
    }
    List<String> __fields = this.__parser.parseRecord(__record);
    __loadFromFields(__fields);
  }

  private void __loadFromFields(List<String> fields) {
    Iterator<String> __it = fields.listIterator();
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.start_time = null; } else {
      this.start_time = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.msisdn = null; } else {
      this.msisdn = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.cell_id = null; } else {
      this.cell_id = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.rat = null; } else {
      this.rat = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.cei_score = null; } else {
      this.cei_score = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.old_cei_score = null; } else {
      this.old_cei_score = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.imsi = null; } else {
      this.imsi = Long.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  private void __loadFromFields0(Iterator<String> __it) {
    String __cur_str = null;
    try {
    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.start_time = null; } else {
      this.start_time = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.msisdn = null; } else {
      this.msisdn = Long.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.cell_id = null; } else {
      this.cell_id = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null")) { this.rat = null; } else {
      this.rat = __cur_str;
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.cei_score = null; } else {
      this.cei_score = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.old_cei_score = null; } else {
      this.old_cei_score = Integer.valueOf(__cur_str);
    }

    __cur_str = __it.next();
    if (__cur_str.equals("null") || __cur_str.length() == 0) { this.imsi = null; } else {
      this.imsi = Long.valueOf(__cur_str);
    }

    } catch (RuntimeException e) {    throw new RuntimeException("Can't parse input data: '" + __cur_str + "'", e);    }  }

  public Object clone() throws CloneNotSupportedException {
    QueryResult o = (QueryResult) super.clone();
    return o;
  }

  public void clone0(QueryResult o) throws CloneNotSupportedException {
  }

  public Map<String, Object> getFieldMap() {
    Map<String, Object> __sqoop$field_map = new HashMap<String, Object>();
    __sqoop$field_map.put("start_time", this.start_time);
    __sqoop$field_map.put("msisdn", this.msisdn);
    __sqoop$field_map.put("cell_id", this.cell_id);
    __sqoop$field_map.put("rat", this.rat);
    __sqoop$field_map.put("cei_score", this.cei_score);
    __sqoop$field_map.put("old_cei_score", this.old_cei_score);
    __sqoop$field_map.put("imsi", this.imsi);
    return __sqoop$field_map;
  }

  public void getFieldMap0(Map<String, Object> __sqoop$field_map) {
    __sqoop$field_map.put("start_time", this.start_time);
    __sqoop$field_map.put("msisdn", this.msisdn);
    __sqoop$field_map.put("cell_id", this.cell_id);
    __sqoop$field_map.put("rat", this.rat);
    __sqoop$field_map.put("cei_score", this.cei_score);
    __sqoop$field_map.put("old_cei_score", this.old_cei_score);
    __sqoop$field_map.put("imsi", this.imsi);
  }

  public void setField(String __fieldName, Object __fieldVal) {
    if (!setters.containsKey(__fieldName)) {
      throw new RuntimeException("No such field:"+__fieldName);
    }
    setters.get(__fieldName).setField(__fieldVal);
  }

}
