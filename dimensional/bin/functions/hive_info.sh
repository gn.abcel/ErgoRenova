#!/bin/bash

function hive_info_get_nrows {
	HIVE_LOG=$1
	DATABASE_NAME=$(echo $2 | tr '[:upper:]' '[:lower:]')
	TABLE_NAME=$(echo $3 | tr '[:upper:]' '[:lower:]')
	PARTITION=$(echo $4 | tr '[:upper:]' '[:lower:]')
	DATE=$5
	
	SEARCH_PATTERN="INFO  : Partition "$DATABASE_NAME"."$TABLE_NAME"{"$PARTITION"="$DATE"} stats:"
	NROWS=$(cat $HIVE_LOG | grep "$SEARCH_PATTERN" | cut -d ',' -f2 | cut -d '=' -f2 | head -n 1)
	
	if [ "$NROWS" == "" ]; then
		SEARCH_PATTERN="INFO  : Table "$DATABASE_NAME"."$TABLE_NAME" stats:"
		NROWS=$(cat $HIVE_LOG | grep "$SEARCH_PATTERN" | cut -d ',' -f2 | cut -d '=' -f2 | head -n 1)
	fi
	
	echo $NROWS
}

function hive_info_get_table_min_max_dates {
	DATABASE_NAME=$(echo $1 | tr '[:upper:]' '[:lower:]')
	TABLE_NAME=$(echo $2 | tr '[:upper:]' '[:lower:]')
	COLUMN_NAME=$(echo $3 | tr '[:upper:]' '[:lower:]')
	
	SET_HIVE_QUERY="-e \"SELECT CONCAT(MIN(DATE("$COLUMN_NAME")),'_',MAX(DATE("$COLUMN_NAME"))) FROM "$DATABASE_NAME"."$TABLE_NAME" LIMIT 1;\""
	MIN_MAX_DATE=$(eval "$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_OUTPUT_CONF $SET_HIVE_QUERY")
	
	echo $MIN_MAX_DATE
}

function hive_info_get_table_last_partition {
	DATABASE_NAME=$(echo $1 | tr '[:upper:]' '[:lower:]')
	TABLE_NAME=$(echo $2 | tr '[:upper:]' '[:lower:]')
	COLUMN_NAME=$(echo $3 | tr '[:upper:]' '[:lower:]')
	
	SET_HIVE_QUERY="-e \"SELECT MAX("$COLUMN_NAME") FROM "$DATABASE_NAME"."$TABLE_NAME";\""
	MAX_PARTITION=$(eval "$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_OUTPUT_CONF $SET_HIVE_QUERY")
	
	echo $MAX_PARTITION
}

function hive_info_get_table_path {
	DATABASE_NAME=$(echo $1 | tr '[:upper:]' '[:lower:]')
	TABLE_NAME=$(echo $2 | tr '[:upper:]' '[:lower:]')
	
	SET_HIVE_QUERY="-e \"SHOW CREATE TABLE "$DATABASE_NAME"."$TABLE_NAME";\""
	PATH=$(eval "$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_OUTPUT_CONF $SET_HIVE_QUERY" | grep -a -Po "://.+?\K/.*(?=')")
	
	echo $PATH
}
