#!/bin/bash

function send_mail {
	CONTENT_TYPE=$1
	LOG_FILE=$2
	SUBJECT=$3
	
	MAIL_SENTENCE="mutt -e \"set from='"$mail_from" <"$mail_from">' smtp_url=smtp://"$mail_smtp_server
	case $CONTENT_TYPE in 
		"HTML") 
			MAIL_SENTENCE=$MAIL_SENTENCE" content_type=text/html\"";;
		*) 
			MAIL_SENTENCE=$MAIL_SENTENCE"\"";;
	esac
	MAIL_SENTENCE=$MAIL_SENTENCE" -s \""$SUBJECT"\""
	if ! [ -z "$4" ]; then
		ATTACH=$4
		test -e $ATTACH
		if [ ${PIPESTATUS[0]} -eq 0 ] ; then
			MAIL_SENTENCE=$MAIL_SENTENCE" -a "$ATTACH
		fi
	fi
	if ! [ -z "$mail_cco" ]; then
		MAIL_SENTENCE=$MAIL_SENTENCE" -b "$mail_cco
	fi
	MAIL_SENTENCE=$MAIL_SENTENCE" -- "$mail_to" < "$LOG_FILE
	
	eval $MAIL_SENTENCE
}
