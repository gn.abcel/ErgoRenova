CREATE TABLE tableros.validator_status (
  process_name string,
  data_date date,
  day_of_week string,
  table_name string,
  control_description string,
  current_value double,
  current_median double,
  audit_timestamp timestamp )
STORED AS ORC;

CREATE VIEW tableros.validator_status_vw AS
SELECT
  a.process_name,
  a.data_date,
  a.day_of_week,
  a.table_name,
  a.control_description,
  a.current_value,
  a.current_median
FROM
  tableros.validator_status a
INNER JOIN
  (SELECT
     process_name,
     table_name, 
     data_date, 
     control_description, 
     MAX(audit_timestamp) audit_timestamp 
   FROM
     tableros.validator_status
   GROUP BY process_name, table_name, data_date, control_description) b
ON
  a.process_name = b.process_name AND
  a.data_date = b.data_date AND
  a.table_name = b.table_name AND
  a.control_description = b.control_description AND
  a.audit_timestamp = b.audit_timestamp;
