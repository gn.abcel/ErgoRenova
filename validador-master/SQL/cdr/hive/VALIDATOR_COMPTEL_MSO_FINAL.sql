-- SET tez.queue.name=${TEZ_QUEUE};
SET hive.groupby.orderby.position.alias=True;

DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp4;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp5;

-- Controles sobre la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp1 STORED AS ORC AS
SELECT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  COUNT(1) value_calls
FROM
  ${DBNAME_TRAFICO}.comptel_mso_final
WHERE
  fecha = ${CURRENT_DATE_N}
GROUP BY 1;

-- Fechas anteriores a la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp2 STORED AS ORC AS
SELECT DISTINCT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  fecha data_date_n,
  DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd"), 'u') day_of_week
FROM
  ${DBNAME_TRAFICO}.comptel_mso_final
WHERE
  fecha < ${CURRENT_DATE_N};

-- Fechas anteriores a la fecha a evaluar, correspondiente a dias no feriados
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp3 STORED AS ORC AS
SELECT
  a.data_date,
  a.data_date_n,
  a.day_of_week,
  RANK() OVER (PARTITION BY a.day_of_week ORDER BY DATEDIFF(DATE('${CURRENT_DATE}'), a.data_date) ASC) rnk
FROM
  ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp2 a
INNER JOIN 
  (SELECT 
     fecha_fc data_date
   FROM 
     ${DBNAME_TABLEROS}.dim_calendario
   WHERE 
     DATE(fecha_fc) < DATE('${CURRENT_DATE}') AND
     feriado_fl = 0) b
ON
  a.data_date = b.data_date;

-- Muestreo de cuatro semanas anteriores a la actual, tomando el mismo dia de la semana que el dia evaluado
INSERT OVERWRITE TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp3
SELECT
  data_date,
  data_date_n,
  day_of_week,
  0 rnk
FROM
  ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp3
WHERE
  rnk <= ${TABLE_SAMPLE_DAYS} AND
  day_of_week = DATE_FORMAT(DATE('${CURRENT_DATE}'), 'u');

-- Obtengo los valores los dias obtenidos
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp4 STORED AS ORC AS
SELECT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  CAST(1 AS BIGINT) value_calls
FROM
  ${DBNAME_TRAFICO}.comptel_mso_final
WHERE
  fecha IN (SELECT data_date_n FROM ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp3);

-- Genero los valores sumarizados de los dias obtenidos
INSERT OVERWRITE TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp4
SELECT
  data_date,
  SUM(value_calls) value_calls
FROM
  ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp4
GROUP BY 1;

-- Genero la mediana para la fecha evaluada con los valores de los dias de semanas anteriores
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp5 STORED AS ORC AS
SELECT
  DATE_FORMAT(data_date, 'u') day_of_week,
  percentile(CAST(value_calls AS BIGINT),0.5) median_calls
FROM
  ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp4
GROUP BY 1;

-- Inserto el registro en la tabla de controles
INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  'comptel_mso_final' table_name,
  'calls' control_description,
  CAST(a.value_calls AS DOUBLE) current_value,
  b.median_calls current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp5 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');

DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp4;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_mso_final_tmp5;
