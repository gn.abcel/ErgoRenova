SET hive.groupby.orderby.position.alias=True;

DROP TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp3;

-- Controles sobre la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp1 STORED AS ORC AS
SELECT
  DATE('${CURRENT_DATE}') data_date,
  COUNT(1) value_rows
FROM
  ${DBNAME_TRAFICO}.sdr_dyn_cea_cei_cei_cell_usr_1day
WHERE
  cast(data_date as int) = ${CURRENT_DATE_N}
GROUP BY 1;

-- Fechas anteriores a la fecha a evaluar
-- Obtengo los valores los dias obtenidos
-- Genero los valores sumarizados de los dias obtenidos
CREATE TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp2 STORED AS ORC AS
SELECT
  FROM_UNIXTIME(UNIX_TIMESTAMP(data_date,'yyyyMMdd'),'yyyy-MM-dd') data_date,
  CAST(COUNT(1) AS DECIMAL) value_rows
FROM
  ${DBNAME_TRAFICO}.sdr_dyn_cea_cei_cei_cell_usr_1day
WHERE
  cast(data_date as int) < ${CURRENT_DATE_N} AND
  cast(data_date as int) >= ${DATE_FROM_N}
GROUP BY 1;

-- Genero la mediana para la fecha evaluada con los valores de los dias de semanas anteriores
CREATE TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp3 STORED AS ORC AS
SELECT
  DATE_FORMAT(DATE(data_date), 'u') day_of_week,
  percentile(CAST(value_rows AS BIGINT),0.5) median_rows
FROM
  ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp2
GROUP BY 1;

-- Inserto el registro en la tabla de controles
INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  'sdr_dyn_cea_cei_cei_cell_usr_1day' table_name,
  'rows' control_description,
  CAST(a.value_rows AS DOUBLE) current_value,
  b.median_rows current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp3 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');

DROP TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_sdr_dyn_cea_cei_cei_cell_usr_1day_tmp3;
