-- geo_antenas
-- geo_departamentos
-- geo_estados_cel
-- geo_localidades
-- geo_localizacion_antenas_mv
-- geo_provincias
-- geo_reclamos
-- geo_red
-- geo_registro_consultas4g
-- geo_rel_ant_tipo_evento
-- geo_rel_ant_vec
-- geo_rel_rec_ant
-- geo_servicio
-- geo_tipo_evento_ant

-- SET tez.queue.name=${TEZ_QUEUE};
SET hive.groupby.orderby.position.alias=True;

--
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;

-- Controles sobre la fecha a evaluar
--tmp1
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 STORED AS ORC AS
SELECT DATE($CURRENT_DATE),
COUNT(1) value_rows,
from ${DBNAME_RECLAMOS}.${TABLE_NAME};

--tmp2
create table ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2 STORED AS ORC AS
select
data_date,
current_value value_rows
from
${DBNAME_TABLEROS}.validator_status
where
process_name = '${MODE}' and
table_name = '${TABLE_NAME}' and
date(data_date) >= '$DATE_FROM' and
date(data_date) < date('${CURRENT_DATE}');


CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3 STORED AS ORC AS
SELECT
  DATE_FORMAT(DATE('${CURRENT_DATE}'), 'u') day_of_week,
  percentile(CAST(value_rows AS BIGINT),0.5) median_rows
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2
GROUP BY 1;



INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  '${TABLE_NAME}' table_name,
  'rows' control_description,
  CAST(a.value_rows AS DOUBLE) current_value,
  b.median_rows current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');


DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;
