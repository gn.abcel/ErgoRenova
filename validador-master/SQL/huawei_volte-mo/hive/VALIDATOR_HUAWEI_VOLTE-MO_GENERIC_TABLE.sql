SET hive.groupby.orderby.position.alias=True;

DROP TABLE if exists ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE if exists ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE if exists ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;

-- Controles sobre la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 STORED AS ORC AS
SELECT
  FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING),'yyyyMMdd'),'yyyy-MM-dd') data_date,
  COUNT(1) value_rows
FROM
  ${DBNAME_TRAFICO}.${TABLE_NAME}
WHERE
  FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING),'yyyyMMdd'),'yyyy-MM-dd') = '${CURRENT_DATE}'
GROUP BY 1;

-- Fechas anteriores a la fecha a evaluar
-- Obtengo los valores los dias obtenidos
-- Genero los valores sumarizados de los dias obtenidos
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2 STORED AS ORC AS
SELECT
  FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING),'yyyyMMdd'),'yyyy-MM-dd') data_date,
  CAST(COUNT(1) AS DECIMAL) value_rows
FROM
  ${DBNAME_TRAFICO}.${TABLE_NAME}
WHERE
  FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING),'yyyyMMdd'),'yyyy-MM-dd') < '${CURRENT_DATE}' AND
  FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING),'yyyyMMdd'),'yyyy-MM-dd') >= DATE('${DATE_FROM}')
GROUP BY 1;

-- Genero la mediana para la fecha evaluada con los valores de los dias de semanas anteriores
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3 STORED AS ORC AS
SELECT
  DATE_FORMAT(DATE(data_date), 'u') day_of_week,
  percentile(CAST(value_rows AS BIGINT),0.5) median_rows
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2
GROUP BY 1;

-- Inserto el registro en la tabla de controles
INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  '${TABLE_NAME}' table_name,
  'rows' control_description,
  CAST(a.value_rows AS DOUBLE) current_value,
  b.median_rows current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');

DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;
