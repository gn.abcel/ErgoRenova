--bots_novum_movistar_ar
--app_rating_feedback_movistar_ar
--calls_novum_movistar_ar
--novum_base_b2b,novum_parque
--novum_ipcomms_usage
--novum_support_chat_survey_b2b
--novum_active_users
--novum_account_novum

-- SET tez.queue.name=${TEZ_QUEUE};
SET hive.groupby.orderby.position.alias=True;

--
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;

-------------------------------------------------------------------------------------
-- Se cuentan los registros sobre la fecha a evaluar
-------------------------------------------------------------------------------------

CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 STORED AS ORC AS
SELECT DATE('${CURRENT_DATE}') data_date,
COUNT(1) value_rows
from ${DBNAME_TRAFICO}.${TABLE_NAME};

-------------------------------------------------------------------------------------
--Se consulta la tabla historica para el calculo de la mediana, se genera la tmp2
-------------------------------------------------------------------------------------

create table ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2 STORED AS ORC AS
select
data_date,
current_value value_rows
from
${DBNAME_TABLEROS}.validator_status
where
process_name = '${MODE}' and
table_name = '${TABLE_NAME}' and
date(data_date) >= date('$DATE_FROM') and
date(data_date) < date('${CURRENT_DATE}');

-------------------------------------------------------------------------------------
--Se calculo la mediana en base a los n dias solicitados, se genera la tabla tmp3--
-------------------------------------------------------------------------------------

CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3 STORED AS ORC AS
SELECT
  DATE_FORMAT(DATE('${CURRENT_DATE}'), 'u') day_of_week,
  percentile(CAST(value_rows AS BIGINT),0.5) median_rows
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2
GROUP BY 1;

-------------------------------------------------------------------------------------
--Se insertan el conteo de la fecha y la mediana a la tabla historica --
-------------------------------------------------------------------------------------

INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  '${TABLE_NAME}' table_name,
  'rows' control_description,
  CAST(a.value_rows AS DOUBLE) current_value,
  b.median_rows current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');


DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;
