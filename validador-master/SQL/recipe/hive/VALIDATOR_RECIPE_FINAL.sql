-- SET tez.queue.name=${TEZ_QUEUE};
SET hive.groupby.orderby.position.alias=True;

DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp4;

-- Controles sobre la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp1 STORED AS ORC AS
SELECT
  DATE(start_local_utc) data_date,
  COUNT(1) value_rows,
  CAST(SUM(last_duration_seconds) AS BIGINT) value_last_duration_seconds
FROM
  ${DBNAME_TRAFICO}.recipe_final
WHERE
  aym = ${CURRENT_MONTH_YEAR_N} AND
  DATE(start_local_utc) = DATE('${CURRENT_DATE}')
GROUP BY 1;

-- Fechas anteriores a la fecha a evaluar
-- Obtengo los valores los dias obtenidos
CREATE TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp2 STORED AS ORC AS
SELECT
  DATE(start_local_utc) data_date,
  CAST(1 AS BIGINT) value_rows,
  CAST(last_duration_seconds AS BIGINT) value_last_duration_seconds
FROM
  ${DBNAME_TRAFICO}.recipe_final
WHERE
  aym BETWEEN ${MONTH_YEAR_FROM_N} AND ${CURRENT_MONTH_YEAR_N} AND
  DATE(start_local_utc) >= DATE('${DATE_FROM}') AND
  DATE(start_local_utc) < DATE('${CURRENT_DATE}');

-- Genero los valores sumarizados de los dias obtenidos
INSERT OVERWRITE TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp2
SELECT
  data_date,
  SUM(value_rows) value_rows,
  CAST(SUM(value_last_duration_seconds) AS BIGINT) value_last_duration_seconds
FROM
  ${DBNAME_TABLEROS}.validator_recipe_final_tmp2
GROUP BY 1;

-- Genero la mediana para la fecha evaluada con los valores de los dias de semanas anteriores
CREATE TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp3 STORED AS ORC AS
SELECT
  DATE_FORMAT(DATE('${CURRENT_DATE}'), 'u') day_of_week,
  percentile(CAST(value_rows AS BIGINT),0.5) median_rows
FROM
  ${DBNAME_TABLEROS}.validator_recipe_final_tmp2
GROUP BY 1;

CREATE TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp4 STORED AS ORC AS
SELECT
  DATE_FORMAT(DATE('${CURRENT_DATE}'), 'u') day_of_week,
  percentile(CAST(value_last_duration_seconds AS BIGINT),0.5) median_last_duration_seconds
FROM
  ${DBNAME_TABLEROS}.validator_recipe_final_tmp2
GROUP BY 1;

-- Inserto el registro en la tabla de controles
INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  'recipe_final' table_name,
  'rows' control_description,
  CAST(a.value_rows AS DOUBLE) current_value,
  b.median_rows current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_recipe_final_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_recipe_final_tmp3 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');

INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  'recipe_final' table_name,
  'last_duration_seconds' control_description,
  CAST(a.value_last_duration_seconds AS DOUBLE) current_value,
  b.median_last_duration_seconds current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_recipe_final_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_recipe_final_tmp4 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');

DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_recipe_final_tmp4;
