-- SET tez.queue.name=${TEZ_QUEUE};
SET hive.groupby.orderby.position.alias=True;

DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp4;

-- Controles sobre la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 STORED AS ORC AS
SELECT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  COUNT(1) value_rows
FROM
  ${DBNAME_ELEMENTOS_RED}.${TABLE_NAME}
WHERE
  fecha = ${CURRENT_DATE_N}
GROUP BY 1;

-- Fechas anteriores a la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2 STORED AS ORC AS
SELECT DISTINCT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  fecha data_date_n,
  DATE_FORMAT(DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")), 'u') day_of_week
FROM
  ${DBNAME_ELEMENTOS_RED}.${TABLE_NAME}
WHERE
  fecha < ${CURRENT_DATE_N} AND
  fecha >= ${DATE_FROM_N};

-- Obtengo los valores los dias obtenidos
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3 STORED AS ORC AS
SELECT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  CAST(1 AS BIGINT) value_rows
FROM
  ${DBNAME_ELEMENTOS_RED}.${TABLE_NAME}
WHERE
  fecha IN (SELECT data_date_n FROM ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2);

-- Genero los valores sumarizados de los dias obtenidos
INSERT OVERWRITE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3
SELECT
  data_date,
  SUM(value_rows) value_rows
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3
GROUP BY 1;

-- Genero la mediana para la fecha evaluada con los valores de los dias de semanas anteriores
CREATE TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp4 STORED AS ORC AS
SELECT
  DATE_FORMAT(DATE('${CURRENT_DATE}'), 'u') day_of_week,
  percentile(CAST(value_rows AS BIGINT),0.5) median_rows
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3
GROUP BY 1;

-- Inserto el registro en la tabla de controles
INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  '${TABLE_NAME}' table_name,
  'rows' control_description,
  CAST(a.value_rows AS DOUBLE) current_value,
  b.median_rows current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp4 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');

DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_${TABLE_NAME}_tmp4;
