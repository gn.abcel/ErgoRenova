-- SET tez.queue.name=${TEZ_QUEUE};
SET hive.groupby.orderby.position.alias=True;

DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp4;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp5;

-- Controles sobre la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp1 STORED AS ORC AS
SELECT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  SUM(NVL(kb,0))/1099511627776 value_terabytes --1099511627776=1024^4
FROM
  ${DBNAME_TRAFICO}.comptel_xdr_consolidado
WHERE
  fecha = ${CURRENT_DATE_N}
GROUP BY 1;

-- Fechas anteriores a la fecha a evaluar
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp2 STORED AS ORC AS
SELECT DISTINCT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  fecha data_date_n,
  DATE_FORMAT(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd"), 'u') day_of_week
FROM
  ${DBNAME_TRAFICO}.comptel_xdr_consolidado
WHERE
  fecha < ${CURRENT_DATE_N};

-- Fechas anteriores a la fecha a evaluar, correspondiente a dias no feriados
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp3 STORED AS ORC AS
SELECT
  a.data_date,
  a.data_date_n,
  a.day_of_week,
  RANK() OVER (PARTITION BY a.day_of_week ORDER BY DATEDIFF(DATE('${CURRENT_DATE}'), a.data_date) ASC) rnk
FROM
  ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp2 a
INNER JOIN 
  (SELECT 
     fecha_fc data_date
   FROM 
     ${DBNAME_TABLEROS}.dim_calendario
   WHERE 
     DATE(fecha_fc) < DATE('${CURRENT_DATE}') AND
     feriado_fl = 0) b
ON
  a.data_date = b.data_date;

-- Muestreo de cuatro semanas anteriores a la actual, tomando el mismo dia de la semana que el dia evaluado
INSERT OVERWRITE TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp3
SELECT
  data_date,
  data_date_n,
  day_of_week,
  0 rnk
FROM
  ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp3
WHERE
  rnk <= ${TABLE_SAMPLE_DAYS} AND
  day_of_week = DATE_FORMAT(DATE('${CURRENT_DATE}'), 'u');

-- Obtengo los valores los dias obtenidos
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp4 STORED AS ORC AS
SELECT
  DATE(FROM_UNIXTIME(UNIX_TIMESTAMP(CAST(fecha AS STRING), "yyyyMMdd"), "yyyy-MM-dd")) data_date,
  kb value_terabytes
FROM
  ${DBNAME_TRAFICO}.comptel_xdr_consolidado
WHERE
  fecha IN (SELECT data_date_n FROM ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp3);

-- Genero los valores sumarizados de los dias obtenidos
INSERT OVERWRITE TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp4
SELECT
  data_date,
  SUM(NVL(value_terabytes,0))/1099511627776 value_terabytes --1099511627776=1024^4
FROM
  ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp4
GROUP BY 1;

-- Genero la mediana para la fecha evaluada con los valores de los dias de semanas anteriores
CREATE TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp5 STORED AS ORC AS
SELECT
  DATE_FORMAT(data_date, 'u') day_of_week,
  percentile(CAST(value_terabytes AS INT),0.5) median_terabytes
FROM
  ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp4
GROUP BY 1;

-- Inserto el registro en la tabla de controles
INSERT INTO TABLE ${DBNAME_TABLEROS}.validator_status
SELECT
  '${MODE}' process_name,
  DATE(a.data_date) data_date,
  b.day_of_week,
  'comptel_xdr_consolidado' table_name,
  'terabytes' control_description,
  CAST(a.value_terabytes AS DOUBLE) current_value,
  b.median_terabytes current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp1 a
INNER JOIN
  ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp5 b
ON
  b.day_of_week = DATE_FORMAT(a.data_date, 'u');

DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp1;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp2;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp3;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp4;
DROP TABLE ${DBNAME_TABLEROS}.validator_comptel_xdr_consolidado_tmp5;
