#!/bin/bash

function log {
	STATUS=$1
	LOG_FILE=$2
	MESSAGE=$3
	
	PREFIX=$(date "+%Y/%m/%d %H:%M:%S")
	
	case $STATUS in 
		0) PREFIX=$PREFIX" - [INF] - ";;
		1) PREFIX=$PREFIX" - [WRN] - ";;
		2) PREFIX=$PREFIX" - [ERR] - ";;
		*) PREFIX=$PREFIX" - ";;
	esac
	
	echo $PREFIX$MESSAGE | tee -a $LOG_FILE
}
