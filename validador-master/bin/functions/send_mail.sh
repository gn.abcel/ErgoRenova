#!/bin/bash

function send_mail {
	CONTENT_TYPE=$1
	TO=$2
	LOG_FILE=$3
	SUBJECT=$4
	
	case $TO in
		"TGT_SUPPORT")
			mail_to=$mail_to_tgt_support;;
		"USER_SUPPORT")
			mail_to=$mail_to_user_support;;
		*)
			mail_to=$mail_to_user_group;;
	esac
	
	MAIL_SENTENCE="mutt -e \"set from='"$mail_from" <"$mail_from">' smtp_url=smtp://"$mail_smtp_server
	case $CONTENT_TYPE in 
		"HTML") 
			MAIL_SENTENCE=$MAIL_SENTENCE" content_type=text/html\"";;
		*) 
			MAIL_SENTENCE=$MAIL_SENTENCE"\"";;
	esac
	MAIL_SENTENCE=$MAIL_SENTENCE" -s \""$SUBJECT"\""
	if ! [ -z "$5" ]; then
		ATTACH=$5
		test -e $ATTACH
		if [ ${PIPESTATUS[0]} -eq 0 ] ; then
			MAIL_SENTENCE=$MAIL_SENTENCE" -a "$ATTACH
		fi
	fi
	if ! [ -z "$mail_cco" ]; then
		MAIL_SENTENCE=$MAIL_SENTENCE" -b "$mail_cco
	fi
	MAIL_SENTENCE=$MAIL_SENTENCE" -- "$mail_to" < "$LOG_FILE
	
	eval $MAIL_SENTENCE
}
