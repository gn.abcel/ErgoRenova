#!/bin/bash

### Se recibe por parametro la variable de fecha de proceso en formato YYYY-MM-DD y se pasa a formato YYYYMMDD.
INPUT_DATE=$1
SUM=0
PROCESS_DATE_1=$(date --date="${INPUT_DATE} - 1 days" +%Y%m%d)
PROCESS_DATE_2=$(date --date="${INPUT_DATE} - 2 days" +%Y%m%d)

### Invocacion del proceso de validacion
/Fuentes/validator/bin/validator.sh acs-daily $PROCESS_DATE_1
VALIDATOR_STATUS_1=$?

### Invocacion del proceso de validacion
/Fuentes/validator/bin/validator.sh acs-daily $PROCESS_DATE_2
VALIDATOR_STATUS_2=$?

CHECK=$((VALIDATOR_STATUS_1 + VALIDATOR_STATUS_2))
### 0: OK (PROCESO + DATOS)
### 1: ERROR (PROCESO)
### 2: ERROR (DATOS)
if [ $CHECK -eq 2 ]; then
	exit 1
elif [ $VALIDATOR_STATUS_2 -eq 1 ]; then
	exit 1
elif [ $CHECK -eq 0 ]; then
	exit 0
else
	exit 2
fi
