#!/bin/bash

### Se recibe por parametro la variable de fecha de proceso en formato YYYY-MM-DD y se pasa a formato YYYYMMDD.
### Como este proceso corre una vez por semana y corre por los últimos 7 días entonces el validador correrá 7 veces.
INPUT_DATE=$1
PROCESS_DATE_1=$(date --date="${INPUT_DATE} - 1 days" +%Y%m%d)
PROCESS_DATE_2=$(date --date="${INPUT_DATE} - 2 days" +%Y%m%d)
PROCESS_DATE_3=$(date --date="${INPUT_DATE} - 3 days" +%Y%m%d)
PROCESS_DATE_4=$(date --date="${INPUT_DATE} - 4 days" +%Y%m%d)
PROCESS_DATE_5=$(date --date="${INPUT_DATE} - 5 days" +%Y%m%d)
PROCESS_DATE_6=$(date --date="${INPUT_DATE} - 6 days" +%Y%m%d)
PROCESS_DATE_7=$(date --date="${INPUT_DATE} - 7 days" +%Y%m%d)

### Invocacion del proceso de validacion
/Fuentes/validator/bin/validator.sh acs-weekly $PROCESS_DATE_1
VALIDATOR_STATUS_1=$?

/Fuentes/validator/bin/validator.sh acs-weekly $PROCESS_DATE_2
VALIDATOR_STATUS_2=$?

/Fuentes/validator/bin/validator.sh acs-weekly $PROCESS_DATE_3
VALIDATOR_STATUS_3=$?

/Fuentes/validator/bin/validator.sh acs-weekly $PROCESS_DATE_4
VALIDATOR_STATUS_4=$?

/Fuentes/validator/bin/validator.sh acs-weekly $PROCESS_DATE_5
VALIDATOR_STATUS_5=$?

/Fuentes/validator/bin/validator.sh acs-weekly $PROCESS_DATE_6
VALIDATOR_STATUS_6=$?

/Fuentes/validator/bin/validator.sh acs-weekly $PROCESS_DATE_7
VALIDATOR_STATUS_7=$?

CHECK=$((VALIDATOR_STATUS_1 + VALIDATOR_STATUS_2 + VALIDATOR_STATUS_3 + VALIDATOR_STATUS_4 + VALIDATOR_STATUS_5 VALIDATOR_STATUS_6 + VALIDATOR_STATUS_7))

if [ $CHECK -eq 7 ]; then
	exit 1
elif [ $VALIDATOR_STATUS_2 -eq 1 ]; then
	exit 1
elif [ $CHECK -eq 0 ]; then
	exit 0
else
	exit 2
fi

### 0: OK (PROCESO + DATOS)
### 1: ERROR (PROCESO)
### 2: ERROR (DATOS)
if [ $CHECK -eq 2 ]; then
	exit 1
elif [ $VALIDATOR_STATUS_7 -eq 1 ]; then
	exit 1
elif [ $CHECK -eq 0 ]; then
	exit 0
else
	exit 2
fi
