#!/bin/bash

### Se recibe por parametro la variable de fecha de proceso en formato YYYY-MM-DD y se pasa a formato YYYYMMDD.
### astro cierran a dia vencido.
INPUT_DATE=$1
PROCESS_DATE=$(date --date="${INPUT_DATE} - 1 days" +%Y%m%d)

### Invocacion del proceso de validacion
/Fuentes/validator/bin/validator.sh astro-main $PROCESS_DATE
VALIDATOR_STATUS=$?

### 0: OK (PROCESO + DATOS)
### 1: ERROR (PROCESO)
### 2: ERROR (DATOS)
exit $VALIDATOR_STATUS
