#!/bin/bash

### Se recibe por parametro la variable de fecha de proceso en formato YYYY-MM-DD y se pasa a formato YYYYMMDD.
### cei_sdr_dyn_cea_cei_z_cell_usr_sms_1day cierra a dia vencido.
INPUT_DATE=$1
PROCESS_DATE=$(date --date="${INPUT_DATE} - 1 days" +%Y%m%d)

### Invocacion del proceso de validacion
/Fuentes/validator/bin/validator.sh cei_sdr_dyn_cea_cei_z_cell_usr_sms_1day $PROCESS_DATE
VALIDATOR_STATUS=$?

### 0: OK (PROCESO + DATOS)
### 1: ERROR (PROCESO)
### 2: ERROR (DATOS)
exit $VALIDATOR_STATUS
