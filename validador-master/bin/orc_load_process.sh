#!/bin/bash
. $APP_DIR"/"$PROCESS_NAME"_par"
. $APP_DIR"/"$MODE"_par"
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_date.conf"
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_hive.conf"
. $PATH_FUNCTIONS/log.sh
#. $PATH_FUNCTIONS/hive_info.sh

log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Se comenzara con la carga de las tablas ORC: $ORC_TABLE_LIST."

### Verifico que si existen scripts particulares para cada tabla o se trata de un script generico
GENERIC_SCRIPT="VALIDATOR_"$(echo $MODE | tr '[:lower:]' '[:upper:]')"_GENERIC_TABLE"
if [ "$ORC_TABLE_LIST" == "$GENERIC_SCRIPT" ]; then
	AUX_ORC_TABLE_LIST=$VALIDATION_TABLE_LIST
else
	AUX_ORC_TABLE_LIST=$ORC_TABLE_LIST
fi

### Carga de tablas orc desde el listado de tablas del archivo de configuracion
COUNTER=0
PROCESS_STATUS=0
# Cantidad de variables
QUANTITY_VARIABLES=$(($(echo "$AUX_ORC_TABLE_LIST" | tr -cd ',' | wc -c)+1))
# Se recorre la lista de variables
while [ $COUNTER -lt $QUANTITY_VARIABLES ]; do
	COUNTER=$[COUNTER+1]
	
	# Se guarda en la variable el nombre de la tabla ORC
	ORC_TABLE_NAME=$(echo $AUX_ORC_TABLE_LIST | cut -d ',' -f$COUNTER)
	
	# Se asigna el nombre de la tabla a mostrar en el log
	if [ "$ORC_TABLE_LIST" == "$GENERIC_SCRIPT" ]; then
		DISPLAY_NAME="VALIDATOR_"$(echo $ORC_TABLE_NAME | tr '[:lower:]' '[:upper:]')
	else
		DISPLAY_NAME=$ORC_TABLE_NAME
	fi
	
	# Se genera el nombre del log de la tabla a procesar
	TABLE_LOG_FILE=$TABLE_LOG_FILE_PREFIX$DISPLAY_NAME$TABLE_LOG_FILE_SUFIX
	
	log 0 "$PATH_LOG/$TABLE_LOG_FILE" "Inicio de carga de la tabla $DISPLAY_NAME."
	
	# Se agrega la variable de hive de nombre de tabla
	SET_HIVE_VAR_TABLE_NAME=" --hivevar TABLE_NAME="$ORC_TABLE_NAME
	
	# Seteo del script SQL a utilizar
	if [ "$ORC_TABLE_LIST" == "$GENERIC_SCRIPT" ]; then
		SET_SQL_SCRIPT=$PATH_SQL/$MODE/hive/$GENERIC_SCRIPT.sql
	else
		SET_SQL_SCRIPT=$PATH_SQL/$MODE/hive/$ORC_TABLE_NAME.sql
	fi
	
	# Se ejecuta el script de insert a ORC
	echo $SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS $SET_HIVE_VAR_TABLE_NAME -f $SET_SQL_SCRIPT | tee -a $PATH_LOG/$SCREEN_LOG_FILE
	echo $SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS $SET_HIVE_VAR_TABLE_NAME -f $SET_SQL_SCRIPT | tee -a $PATH_LOG/$TABLE_LOG_FILE
	
	$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS $SET_HIVE_VAR_TABLE_NAME -f $SET_SQL_SCRIPT >> $PATH_LOG/$TABLE_LOG_FILE 2>&1
	
	if ! [ $? -eq 0 ]; then
		log 2 "$PATH_LOG/$TABLE_LOG_FILE" "Se produjo un error en la carga de la tabla $DISPLAY_NAME."
		log 0 "$PATH_LOG/$TABLE_LOG_FILE" "Fin de carga de la tabla $DISPLAY_NAME."
		log 2 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $DISPLAY_NAME: Se produjo un error durante la carga de la tabla ORC."
		# Notifico el error para salir del ciclo
		PROCESS_STATUS=1
		break
	fi
	
	log 0 "$PATH_LOG/$TABLE_LOG_FILE" "Fin de carga de la tabla $DISPLAY_NAME."
	log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $DISPLAY_NAME: La tabla ORC se proceso correctamente."
done

exit $PROCESS_STATUS
