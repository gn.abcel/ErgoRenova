# -*- coding: utf-8 -*-

### Imports de librerias
import sys
import csv
import os
import yaml
import ast
from pyspark import SparkContext, SparkConf
from pyspark.sql import HiveContext
from decimal import Decimal

### Definicion de funciones
def rowListToDictList(rowList):
	dictList = list()
	for row in rowList:
		dictList.append(row.asDict())
	return dictList

def getControlValuesDict(controlName, controlValuesDictList):
	controlValuesDict = {}
	for controlValues in controlValuesDictList:
		if controlValues['control_description'] == controlName:
			controlValuesDict = controlValues
	return controlValuesDict

def getIntervalList(median, rateList):
	min_value = round(median * (1 + rateList[0]), 2)
	max_value = round(median * (1 + rateList[1]), 2)
	intervalList = [min_value, max_value]
	return intervalList

def evaluateInteval(value, intervalList):
	booleanVar = (intervalList[0] <= value <= intervalList[1])
	return booleanVar

def reportMessage(current_date, mode, table, controlValuesDict, intervalList, status):
	if (controlValuesDict == "NI") or (intervalList == "NI"):
		message = current_date+';'+mode+';'+table+';NOT INFORMED;Mediana: NOT INFORMED;Intervalo de evaluacion: NOT INFORMED;Valor: NOT INFORMED;'+status
	else:
		message = current_date+';'+mode+';'+table+';'+controlValuesDict['control_description']+';Mediana: '+str(controlValuesDict['current_median'])+';Intervalo de evaluacion: '
		if (status == "OK") or (status == "WARN"):
			message += '['+str(intervalList[0])+','+str(intervalList[1])+'];Valor: '+str(controlValuesDict['current_value'])+';'+status
		else:
			message += 'x < '+str(intervalList[0])+' & x > '+str(intervalList[1])+';Valor: '+str(controlValuesDict['current_value'])+';'+status
	return message

def getEvaluationIntervals(control, controlValuesDict):
	# Intervalo de aceptacion
	okIntervalList = getIntervalList(controlValuesDict['current_median'], control['definition']['interval']['ok'])
	# Intervalo de warning y error
	if control['definition']['type'] == "error":
		warnLowerIntervalList = getIntervalList(controlValuesDict['current_median'], control['definition']['interval']['warn'][0])
		warnUpperIntervalList = getIntervalList(controlValuesDict['current_median'], control['definition']['interval']['warn'][1])
		errorIntervalList = [warnLowerIntervalList[0], warnUpperIntervalList[1]]
	else:
		warnLowerIntervalList = warnUpperIntervalList = [0, 0]
		errorIntervalList = okIntervalList
	return okIntervalList, warnLowerIntervalList, warnUpperIntervalList, errorIntervalList

def evaluateControl(current_date, mode, table, control, controlValuesDictList):
	warn = error = 0
	varDataList = list()
	
	### Obtengo los valores correspondientes al control
	controlValuesDict = getControlValuesDict(control['name'], controlValuesDictList)
	### Verifico que el control este incluido en la información obtenida de HIVE
	if not controlValuesDict:
		error += 1
		varDataList.append([reportMessage(current_date, mode, table, "NI", "NI", "ERROR")])
	else:
		### Preparo los margenes para el chequeo
		okIntervalList, warnLowerIntervalList, warnUpperIntervalList, errorIntervalList = getEvaluationIntervals(control, controlValuesDict)
		
		### Se aplica el control
		if evaluateInteval(controlValuesDict['current_value'], okIntervalList):
			varDataList.append([reportMessage(current_date, mode, table, controlValuesDict, okIntervalList, "OK")])
			if control['subcontrols']:
				for subcontrol in control['subcontrols']:
					subwarn, suberror, subVarDataList = evaluateControl(current_date, mode, table, subcontrol, controlValuesDictList)
					warn += subwarn
					error += suberror
					for subVarData in subVarDataList:
						varDataList.append(subVarData)
		elif (control['definition']['type'] == "error") and evaluateInteval(controlValuesDict['current_value'], warnLowerIntervalList):
			varDataList.append([reportMessage(current_date, mode, table, controlValuesDict, warnLowerIntervalList, "WARN")])
			warn += 1
		elif (control['definition']['type'] == "error") and evaluateInteval(controlValuesDict['current_value'], warnUpperIntervalList):
			varDataList.append([reportMessage(current_date, mode, table, controlValuesDict, warnUpperIntervalList, "WARN")])
			warn += 1
		else:
			varDataList.append([reportMessage(current_date, mode, table, controlValuesDict, errorIntervalList, "ERROR")])
			error += 1
	return warn, error, varDataList

def createContext(conf):
	tryCounter=0
	try:
		sc = SparkContext(conf=conf)
	except Exception as e:
		tryCounter += 1
		if tryCounter <= 5:
			createContext(conf)
		else:
			raise e
	return sc

### Modos de ejecucion
mode=sys.argv[1]
table=sys.argv[2]
current_date=sys.argv[3]
report_log_file=sys.argv[4]

### Configuracion SQL Context
conf = SparkConf().setAppName('daily_validation_' + mode + '_' + table + '_' + current_date)
sc = createContext(conf)
sql_context = HiveContext(sc)

try:
	### Directorio del proceso
	os.chdir('/Fuentes/validator')
	### Cargo el archivo de configuracion del proceso
	yml_file = file('conf/validator_parameter.yml', 'r')
	process_config = yaml.load(yml_file)
	### Cargo el archivo de configuracion del modo
	yml_file = file('conf/instances/mode_' + mode + '_parameter.yml', 'r')
	mode_config = yaml.load(yml_file)
	### Cargo el archivo de configuracion del modo
	json_file = open('conf/instances/' + mode + '/table_' + table + '_parameter.json', 'r')
	table_config = ast.literal_eval(((json_file.read()).replace("\n","")).replace("\t",""))
	json_file.close()
except:
	### Error 1 - No se encontraron los archivos de configuracion
	sys.exit(1)

### Obtengo la lista de controles
controls_list = table_config['controls']

### Sentencias SQL
# Armo el listado de controles en formato string separado por comas
control_list_string = ""
for control in controls_list:
	control_list_string += "'" + control['name'] + "',"
	for subcontrol in control['subcontrols']:
		control_list_string += "'" + subcontrol['name'] + "',"
control_list_string = control_list_string[:-1]

# Querys
query_set_db = "USE " + process_config['dbname']['tableros']
query_current_values = "SELECT control_description, current_value, current_median FROM validator_status_vw"
query_current_values += " WHERE process_name = '" + mode + "' AND table_name = '" + table + "' AND DATE(data_date) = DATE('" + current_date + "') AND control_description IN (" + control_list_string + ")"

### Ejecucion de consultas SQL a Hive
sql_context.sql(query_set_db)
current_values_set = sql_context.sql(query_current_values).collect()

### Proceso de chequeo de valores
if len(current_values_set) > 0:
	controlValuesDictList = rowListToDictList(current_values_set)
	warn = error = 0
	varDataList = list()
	for control in controls_list:
		### Se aplica el control
		tmpWarn, tmpError, tmpVarDataList = evaluateControl(current_date, mode, table, control, controlValuesDictList)
		warn += tmpWarn
		error += tmpError
		for tmpVarData in tmpVarDataList:
			varDataList.append(tmpVarData)
		### Escribo el reporte de valores
	with open('/Fuentes/validator/log/'+report_log_file, 'a') as f:
		writer = csv.writer(f, delimiter='\t')
		writer.writerows(varDataList)
	if (error == 0 and warn == 0):
		### Fin exitoso
		sys.exit(0)
	elif error > 0:
		### Error 3 - Existen valores fuera de rango con error (intervalo error)
		sys.exit(3)
	else:
		### Error 4 - Existen valores fuera de rango con error (intervalo warning)
		sys.exit(4)
else:
	### Error 2 - Se produjo un error durante la conexion a Hive
	sys.exit(2)
