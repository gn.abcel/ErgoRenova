#!/bin/bash
. $APP_DIR"/"$PROCESS_NAME"_par"
. $APP_DIR"/"$MODE"_par"
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_date.conf"
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_hive.conf"
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_pyspark.conf"
. $PATH_FUNCTIONS/log.sh

log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Se comenzara con los controles de integridad."
### Carga de tablas a validar desde el listado de tablas del archivo de configuracion
COUNTER=0
PROCESS_STATUS=0
ERROR_COUNT=0
WARN_COUNT=0
# Cantidad de variables
QUANTITY_VARIABLES=$(($(echo "$VALIDATION_TABLE_LIST" | tr -cd ',' | wc -c)+1))
# Se recorre la lista de variables
while [ $COUNTER -lt $QUANTITY_VARIABLES ]; do
	COUNTER=$[COUNTER+1]
	
	# Se guarda en la variable el nombre de la tabla ORC
	ORC_TABLE_NAME=$(echo $VALIDATION_TABLE_LIST | cut -d ',' -f$COUNTER)
	
	log 0 "$PATH_LOG/$TABLE_LOG_FILE" "Inicio de validacion de la tabla $ORC_TABLE_NAME."
	### El script de python toma como argumentos:
	### MODE: procesos disponibles en el config
	### TABLE: tabla del proceso a validar
	### CURRENT_DATE: fecha de evaluacion
	# Validacion de tabla
	SET_SPARK_SCRIPT="\\"$SET_PYTHON_VAL" "$MODE" "$ORC_TABLE_NAME" "$CURRENT_DATE" "$REPORT_LOG_FILE
	eval "$SET_SPARK_CMD $SET_SPARK_CONF $SET_SPARK_JARS $SET_SPARK_SCRIPT"
	EXEC_STATUS=$?
	# Se evalua el mensaje de error devuelto por Python
	case $EXEC_STATUS in 
		0) 
			log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $ORC_TABLE_NAME - Fecha $CURRENT_DATE - Controles OK."
			;;
		2) 
			log 2 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $ORC_TABLE_NAME - Fecha $CURRENT_DATE - Se produjo un error al consultar la tabla para realizar los controles."
			# Error de proceso
			PROCESS_STATUS=1
			break
			;;
		3) 
			log 2 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $ORC_TABLE_NAME - Fecha $CURRENT_DATE - Controles ERROR. Ver archivo adjunto."
			# Error de validacion de datos
			ERROR_COUNT=$[ERROR_COUNT+1]
			;;
		4) 
			log 1 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $ORC_TABLE_NAME - Fecha $CURRENT_DATE - Controles WARN. Ver archivo adjunto."
			# Warning de validacion de datos
			WARN_COUNT=$[WARN_COUNT+1]
			;;
		*) 
			log 2 "$PATH_LOG/$PROCESS_LOG_FILE" "Tabla $ORC_TABLE_NAME - Fecha $CURRENT_DATE - Se produjo un error que no pudo gestionarse."
			# Error de proceso
			PROCESS_STATUS=1
			break
			;;
	esac
done

if [ $ERROR_COUNT -gt 0 ]; then
	PROCESS_STATUS=3
elif [ $WARN_COUNT -gt 0 ]; then
	PROCESS_STATUS=2
elif [ $ERROR_COUNT -eq 0 ] && [ $WARN_COUNT -eq 0 ]; then
	PROCESS_STATUS=0
else
	PROCESS_STATUS=1
fi

exit $PROCESS_STATUS
