#!/bin/bash

### Variable de entorno para ejecuciones nohup
export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

### Seteo nombre de proceso
export PROCESS_NAME="validator"

### Paths utilizados
export CURRENT_DIR=$(dirname ${BASH_SOURCE[0]})
export PATH=/usr/local/bin:$PATH
export PYTHONPATH=/home/hdpadmin/.local/lib/python2.7/site-packages
export APP_DIR=$CURRENT_DIR/..
export PATH_BIN=$APP_DIR/bin
export PATH_FUNCTIONS=$PATH_BIN/functions
export PATH_BIN_CFG=$PATH_BIN/cfg
export PATH_PIG=$PATH_BIN/pig
export PATH_PYTHON=$PATH_BIN/python
export PATH_CONF=$APP_DIR/conf
export PATH_CONF_INSTANCES=$PATH_CONF/instances
export PATH_LOG=$APP_DIR/log
export PATH_SQL=$APP_DIR/SQL
export PATH_TMP=$APP_DIR/tmp

### Imports de funciones
. $PATH_FUNCTIONS/parse_yaml.sh
. $PATH_FUNCTIONS/send_mail.sh
. $PATH_FUNCTIONS/log.sh
. $PATH_FUNCTIONS/file_info.sh
. $PATH_FUNCTIONS/hive_info.sh

### Leyendo Archivo de Configuracion del proceso
PARAMETROS=$PATH_CONF"/"$PROCESS_NAME"_parameter.yml"
parse_yaml $PARAMETROS > $APP_DIR"/"$PROCESS_NAME"_par"
. $APP_DIR"/"$PROCESS_NAME"_par"

### Obtengo el modo de ejecucuón
AVAILABLE_MODES=$modes
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
	export MODE=$1
else
	echo "Debe ingresar uno de los siguientes modos de ejecucion "$AVAILABLE_MODES
	exit 1
fi

### Leyendo Archivo de Configuracion del modo
PARAMETROS=$PATH_CONF_INSTANCES"/mode_"$MODE"_parameter.yml"
parse_yaml $PARAMETROS > $APP_DIR"/"$MODE"_par"
. $APP_DIR"/"$MODE"_par"

### Variables internas de fecha
TMP_VAR="export "$PROCESS_NAME"_DATE=$2"
eval "$TMP_VAR"
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_date.conf"
PROCESS_STATUS=$?
if ! [ $PROCESS_STATUS -eq 0 ]; then
	echo "Debe ingresar la fecha a validar para el proceso "$MODE"."
	exit 1
fi

### Asuntos de mail
ERROR_MSG=$environment" - ERROR - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE
WARN_MSG=$environment" - WARN - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE
FINISH_MSG=$environment" - OK - "$PROCESS_NAME" "$MODE": "$PROCESS_DATE

### Archivos de log
export PROCESS_LOG_FILE="log_"$PROCESS_NAME"_"$MODE"_"$TIMESTAMP_LOG".log"
export SCREEN_LOG_FILE="logAll_"$PROCESS_NAME"_"$MODE"_"$TIMESTAMP_LOG".log"
export TABLE_LOG_FILE_PREFIX="logTable_"$PROCESS_NAME"_"$MODE"_"
export TABLE_LOG_FILE_SUFIX="_"$TIMESTAMP_LOG".log"
export REPORT_LOG_FILE="report_"$PROCESS_NAME"_"$MODE"_"$CURRENT_DATE_N"_"$TIMESTAMP_LOG".log"

### Log que captura toda la salida del proceso
exec > $PATH_LOG/$SCREEN_LOG_FILE 2>&1

### Hive: conexiones
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_hive.conf"

### Sqoop: conexiones
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_sqoop.conf"

### Pig: configuracion
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_pig.conf"

### Archivos Staging
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_file.conf"

### PySpark: configuraciones
. $PATH_BIN_CFG"/"$PROCESS_NAME"_export_vars_pyspark.conf"

### Comienzo del proceso
log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Comienzo del proceso $PROCESS_NAME $MODE."

### Carga de tablas ORC
if ! [ "$ORC_TABLE_LIST" == "" ]; then
	$PATH_BIN/orc_load_process.sh
	PROCESS_STATUS=$?
	if ! [ $PROCESS_STATUS -eq 0 ]; then
		log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Fin del proceso $PROCESS_NAME $MODE."
		# Se envia el log generado por la corrida al usuario
		send_mail "TEXT" "TGT_SUPPORT" "$PATH_LOG/$PROCESS_LOG_FILE" "$ERROR_MSG" "$PATH_LOG/$SCREEN_LOG_FILE"
		exit 1
	fi
fi

if ! [ "$VALIDATION_TABLE_LIST" == "" ]; then
	$PATH_BIN/validate_status.sh
	PROCESS_STATUS=$?
	if [ $PROCESS_STATUS -eq 1 ]; then
		log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Fin del proceso $PROCESS_NAME $MODE."
		# Se envia el log generado por la corrida al usuario
		send_mail "TEXT" "TGT_SUPPORT" "$PATH_LOG/$PROCESS_LOG_FILE" "$ERROR_MSG" "$PATH_LOG/$SCREEN_LOG_FILE"
		exit 1
	fi
fi

# Elimino logs generados por la ejecución del script de PIG
rm -f $APP_DIR/pig_*.log

### Fin del proceso
log 0 "$PATH_LOG/$PROCESS_LOG_FILE" "Fin del proceso $PROCESS_NAME $MODE."

### Se envia el log generado por la corrida al usuario
### Se valida el ultimo estado de ejecucion de sub proceso para validar si hubo warnings ()
if [ $PROCESS_STATUS -eq 2 ]; then
	STATUS_MSG=$WARN_MSG
	send_mail "TEXT" "USER_SUPPORT" "$PATH_LOG/$PROCESS_LOG_FILE" "$STATUS_MSG" "$PATH_LOG/$REPORT_LOG_FILE"
elif [ $PROCESS_STATUS -eq 3 ]; then
	STATUS_MSG=$ERROR_MSG
	send_mail "TEXT" "USER_SUPPORT" "$PATH_LOG/$PROCESS_LOG_FILE" "$STATUS_MSG" "$PATH_LOG/$REPORT_LOG_FILE"
else
	STATUS_MSG=$FINISH_MSG
fi

send_mail "TEXT" "TGT_SUPPORT" "$PATH_LOG/$PROCESS_LOG_FILE" "$STATUS_MSG" "$PATH_LOG/$REPORT_LOG_FILE"

### Se eliminan las configuraciones aplicadas durante la ejecucion
rm -f $APP_DIR"/"$MODE"_par"
rm -f $APP_DIR"/"$PROCESS_NAME"_par"

### Dependiendo si hubo datos validados con error o si el proceso finalizo ok, se envia 2 o 0 respectivamente
if [ $PROCESS_STATUS -eq 3 ]; then
	exit 2
else
	exit 0
fi
