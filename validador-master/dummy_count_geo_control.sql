drop table if exists tableros.dummy_count;
--geo_antenas
CREATE TABLE tableros.dummy_count STORED AS ORC AS
SELECT
  29035 q
FROM
  reclamos.geo_antenas limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_antenas' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_antenas' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_antenas' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_antenas' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_antenas' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_antenas' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_antenas' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count ;
  
--geo_departamentos
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  513 q
FROM
  reclamos.geo_departamentos limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_departamentos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_departamentos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_departamentos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_departamentos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_departamentos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_departamentos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_departamentos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

  
--geo_estados_cel
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  10 q
FROM
  reclamos.geo_estados_cel limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_estados_cel' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_estados_cel' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_estados_cel' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_estados_cel' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_estados_cel' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_estados_cel' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_estados_cel' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count ;

--geo_localidades
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  7226 q
FROM
  reclamos.geo_localidades limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_localidades' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_localidades' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_localidades' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_localidades' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_localidades' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_localidades' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_localidades' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;


--geo_localizacion_antenas_mv
  
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  17715 q
FROM
  reclamos.geo_localizacion_antenas_mv limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_localizacion_antenas_mv' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_localizacion_antenas_mv' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_localizacion_antenas_mv' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_localizacion_antenas_mv' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_localizacion_antenas_mv' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_localizacion_antenas_mv' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count ; 

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_localizacion_antenas_mv' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

--geo_provincias
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
 24 q
FROM
  reclamos.geo_provincias limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_provincias' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_provincias' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_provincias' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_provincias' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_provincias' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_provincias' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_provincias' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;
--geo_reclamos
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  1374930 q
FROM
  reclamos.geo_reclamos limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_reclamos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_reclamos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_reclamos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_reclamos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_reclamos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_reclamos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_reclamos' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

--geo_red
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  4 q
FROM
  reclamos.geo_red limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_red' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_red' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_red' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_red' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_red' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_red' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_red' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

--geo_registro_consultas4g
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  20722 q
FROM
  reclamos.geo_registro_consultas4g limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_registro_consultas4g' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_registro_consultas4g' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_registro_consultas4g' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_registro_consultas4g' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_registro_consultas4g' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_registro_consultas4g' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_registro_consultas4g' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

--geo_rel_ant_tipo_evento
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  5351664 q
FROM
  reclamos.geo_rel_ant_tipo_evento limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_rel_ant_tipo_evento' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_rel_ant_tipo_evento' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_rel_ant_tipo_evento' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_rel_ant_tipo_evento' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_rel_ant_tipo_evento' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_rel_ant_tipo_evento' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_rel_ant_tipo_evento' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

--geo_rel_ant_vec
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  605734 q
FROM
  reclamos.geo_rel_ant_vec limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_rel_ant_vec' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_rel_ant_vec' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_rel_ant_vec' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_rel_ant_vec' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_rel_ant_vec' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_rel_ant_vec' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_rel_ant_vec' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

  
--geo_rel_rec_ant
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  682586 q
FROM
  reclamos.geo_rel_rec_ant limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_rel_rec_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_rel_rec_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_rel_rec_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_rel_rec_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_rel_rec_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_rel_rec_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_rel_rec_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;
--geo_servicio
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
  30 q
FROM
  reclamos.geo_servicio limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_servicio' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_servicio' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_servicio' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_servicio' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_servicio' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_servicio' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_servicio' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

--geo_tipo_evento_ant
truncate table tableros.dummy_count;
insert into table tableros.dummy_count
SELECT
 6 q
FROM
  reclamos.geo_tipo_evento_ant limit 1
  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -1) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -1), 'u') day_of_week,
  'geo_tipo_evento_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -2) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -2), 'u') day_of_week,
  'geo_tipo_evento_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -3) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -3), 'u') day_of_week,
  'geo_tipo_evento_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -4) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -4), 'u') day_of_week,
  'geo_tipo_evento_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;
  
INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -5) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -5), 'u') day_of_week,
  'geo_tipo_evento_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -6) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -6), 'u') day_of_week,
  'geo_tipo_evento_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;

INSERT INTO TABLE tableros.validator_status
SELECT
  'geo' process_name,
  DATE_ADD(DATE('${CURRENT_DATE}'), -7) data_date,
  DATE_FORMAT(DATE_ADD(DATE('${CURRENT_DATE}'), -7), 'u') day_of_week,
  'geo_tipo_evento_ant' table_name,
  'rows' control_description,
  CAST(q AS DOUBLE) current_value,
  CAST(q AS DOUBLE) current_median,
  CURRENT_TIMESTAMP audit_timestamp
FROM
  tableros.dummy_count  ;
